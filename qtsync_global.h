#ifndef QTSYNC_GLOBAL_H
#define QTSYNC_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(QTSYNC_LIBRARY)
#  define QTSYNC_EXPORT Q_DECL_EXPORT
#else
#  define QTSYNC_EXPORT Q_DECL_IMPORT
#endif

#endif // QTSYNC_GLOBAL_H

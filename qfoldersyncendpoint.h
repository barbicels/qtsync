#ifndef QFOLDERSYNCENDPOINT_H
#define QFOLDERSYNCENDPOINT_H

#include <QtCore/QDateTime>
#include <QtCore/QDir>
#include "qfilesyncmanager.h"
#include "qabstractsyncendpoint.h"

class QTSYNC_EXPORT QFolderSyncEndpoint : public QAbstractSyncEndpoint
{
    Q_OBJECT
public:
    explicit QFolderSyncEndpoint(const QString& name, const QSyncEndpointData& data, QObject* parent = nullptr);

    struct QTSYNC_EXPORT QFolderSyncMetadata : public QAbstractSyncMetadata
    {
        friend class QFolderSyncEndpoint;
        QDateTime mLastModified;
        QString mCaseSensitivePath;
    protected:
        virtual void serializeTo(QDataStream& stream) const override;
        virtual void serializeFrom(QDataStream& stream) override;
    };

    class QTSYNC_EXPORT QFolderSyncEndpointState : public QAbstractSyncEndpointState
    {
        friend class QFolderSyncEndpoint;
    public:
        explicit QFolderSyncEndpointState();
        QFolderSyncEndpointState(const QFolderSyncEndpointState& other);
        virtual ~QFolderSyncEndpointState() { }
    public:
        virtual std::unique_ptr<QAbstractSyncEndpointState> clone() const override;
        QDateTime getDateTimeCursor() const;
        void setDateTimeCursor(const QDateTime& dateTime);
    protected:
        enum QFolderSyncEndpointStateVersion { Q_FOLDER_SYNC_ENDPOINT_STATE_VERSION_1 = 1 };
        virtual void serializeTo(QDataStream& stream) const override;
        virtual void serializeFrom(QDataStream& stream) override;
        QMap<QString, QFolderSyncMetadata>& getItemToItemMetadataMap();
    private:
        QDateTime mDateTimeCursor;
        QMap<QString, QFolderSyncMetadata> mItemToItemMetadataMap;
    };

public:
    virtual QString identity() const override;
    virtual bool dateLessThan(const QDateTime& date1, const QDateTime& date2) const override;
    virtual void getItemMetadata(const QString& item, QAbstractSyncMetadata& metadata) override;

protected:
    virtual bool engage(QSyncMode mode) override { Q_UNUSED(mode) return true; }
    virtual bool disengage() override { return true; }
    virtual bool activate() override { return true; }
    virtual bool deactivate() override { return true; }
    virtual bool report(const bool& abort) override;
    virtual void stopActivity() override { }
    virtual void getFileMetadata(const QString& item, QFileSyncManager::QFileSyncMetadata& fileMetadata) const;
    virtual bool provideItem(QAbstractSyncEndpointWorkItem& workItem, const bool& abort) override;
    virtual bool consumeItem(QAbstractSyncEndpointWorkItem& workItem, const bool& abort) override;
    virtual QStringList getCanonicalPaths() override;
    virtual bool isReady() override;

private:
//    constexpr static auto FOLDER_ENDPOINT_NAME = "Folder";
    constexpr static auto FOLDER_ENDPOINT_LOCALITY = 0;
    /* parameters */
    QDir mFolder;
    /* non-persistent state */
    std::map<QString, QString> mDeletedItemToCaseSensitivePathMap;
};

#endif // QFOLDERSYNCENDPOINT_H

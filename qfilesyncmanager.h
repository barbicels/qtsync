#ifndef QFILESYNCMANAGER_H
#define QFILESYNCMANAGER_H

#include "qabstractsyncmanager.h"

class QTSYNC_EXPORT QFileSyncManager : public QAbstractSyncManager
{
    Q_OBJECT
public:
    explicit QFileSyncManager(QObject* parent = nullptr);
    virtual ~QFileSyncManager() override { }

    struct QTSYNC_EXPORT QFileSyncMetadata : public QAbstractSyncMetadata
    {
        friend class QFileSyncManager;
        QFileSyncMetadata();
        bool mIsFolder;
        bool mIsDeleted;
        // The following fields are not defined if isDeleted.
        QDateTime mLastModified;
        QString mVersion;
        std::function<QString()> mDropboxHashGetter;
        qint64 mSize;
        QString mCaseSensitivePath;
    protected:
        virtual void serializeTo(QDataStream& stream) const override { Q_UNUSED(stream) Q_ASSERT(false); }
        virtual void serializeFrom(QDataStream& stream) override { Q_UNUSED(stream) Q_ASSERT(false); }
    };

    class QTSYNC_EXPORT QFileSyncManagerState : public QAbstractSyncManagerState
    {
        friend class QFileSyncManager;
    public:
        explicit QFileSyncManagerState() : QAbstractSyncManagerState() { }
        virtual ~QFileSyncManagerState() { }
    public:
        virtual std::unique_ptr<QAbstractSyncManagerState> clone() const override;
    protected:
        enum QFileSyncManagerStateVersion { Q_FILE_SYNC_MANAGER_STATE_VERSION_1 = 1 };
        virtual void serializeTo(QDataStream& stream) const override;
        virtual void serializeFrom(QDataStream& stream) override;
    };

private:
    const QFileSyncMetadata& castAsFileMetadata(const QString& item, const QAbstractSyncMetadata& metadata) const;

protected:
    virtual std::unique_ptr<QAbstractSyncMetadata> metadataFactory() override;
    virtual bool resolveSyncConflict(const QString& item, std::map<QAbstractSyncEndpoint*, std::unique_ptr<QAbstractSyncMetadata>>& endpointPToItemMetadataUPMap) override;
    virtual bool filterSyncItem(const QString& item, const QAbstractSyncMetadata& metadata, const std::map<QString, std::map<QAbstractSyncEndpoint*, std::unique_ptr<QAbstractSyncMetadata>>>& itemToEndpointPToItemMetadataUPMap) override;
};

#endif // QFILESYNCMANAGER_H

#include <set>
#include "qabstractsyncmanager.h"






// FIX: TEMPORARY
#include "qfoldersyncendpoint.h"
#include "qdropboxsyncendpoint.h"



#include <QtCore/QCoreApplication>


bool allTrue(const std::map<QAbstractSyncEndpoint*, bool>& map)
{
    return std::all_of(map.cbegin(), map.cend(), [] (const std::pair<QAbstractSyncEndpoint*, bool>& x) -> bool { return x.second; });
}

bool allFalse(const std::map<QAbstractSyncEndpoint*, bool>& map)
{
    return std::all_of(map.cbegin(), map.cend(), [] (const std::pair<QAbstractSyncEndpoint*, bool>& x) -> bool { return !x.second; });
}


QAbstractSyncManager::QAbstractSyncManager(QObject* parent)
    : QObject(parent),
      mMode(SyncModeDisabled),
      mSyncTimeout(-1),
      mSyncing(false),
      mAborting(false)
{
    qRegisterMetaType<QSyncMode>("QSyncMode");
    mSyncTimer.callOnTimeout(this, &QAbstractSyncManager::syncTimerExpired);
}

QAbstractSyncManager::~QAbstractSyncManager()
{   qDebug() << "~QAbstractSyncManager\n";
//    while (mSyncing)
//#ifdef Q_OS_IOS
//        QCoreApplication::processEvents(QEventLoop::AllEvents);
//#else
//        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
//#endif
    if (mSyncing)
        qDebug() << "quitting while mSyncing";
    // We must remove all endpoints, to ensure that all threads are stopped.
    removeEndpoints();
}

QAbstractSyncManager::QAbstractSyncManagerState::QAbstractSyncManagerState(const QAbstractSyncManagerState& other)
{
    mEndpointNameToDataMap = other.mEndpointNameToDataMap;
    mEndpointNameToStateMap = other.mEndpointNameToStateMap;
}

void
QAbstractSyncManager::QAbstractSyncManagerState::serializeTo(QDataStream& stream) const
{
    stream.setVersion(QDataStream::Qt_5_12);
    stream << (qint32) Q_SYNC_MANAGER_STATE_VERSION_1;
    // Q_SYNC_MANAGER_STATE_VERSION_1
    stream << mEndpointNameToDataMap
           << mEndpointNameToStateMap;
}

void
QAbstractSyncManager::QAbstractSyncManagerState::serializeFrom(QDataStream& stream)
{
    mEndpointNameToDataMap.clear();
    mEndpointNameToStateMap.clear();
    stream.setVersion(QDataStream::Qt_5_12);
    qint32 version;
    stream >> version;
    switch (version)
    {
    case 0: // null
        return;
    case Q_SYNC_MANAGER_STATE_VERSION_1:
        break;
    default:
        throw QSyncException(std::string("unknown sync manager state version: ") + std::to_string(version));
    }
    if (version >= Q_SYNC_MANAGER_STATE_VERSION_1)
        stream >> mEndpointNameToDataMap
               >> mEndpointNameToStateMap;
}

QByteArray
QAbstractSyncManager::getSerializedState() const
{
    QByteArray serializedState;
    QDataStream stateStream(&serializedState, QIODevice::WriteOnly);
    stateStream << *mStateUP;

    return serializedState;
}

void
QAbstractSyncManager::setSerializedState(QByteArray&& serializedState)
{
    // Take a copy of existing endpoint data, for comparison.
    auto const existingData = mStateUP->mEndpointNameToDataMap;

    QDataStream stateStream(&serializedState, QIODevice::ReadOnly);
    stateStream >> *mStateUP;

    // If new state's data mismatch existing data, nullify state.
    if (mStateUP->mEndpointNameToDataMap != existingData)
    {
        QDataStream nullStream(nullptr, QIODevice::ReadOnly);
        nullStream >> *mStateUP;
    }

    mStateUP->mEndpointNameToDataMap = existingData;
    for (auto const& endpointName : existingData.keys())
    {
        auto& endpoint = *mEndpointNameToEndpointTUPMap[endpointName].get();
        QMutexLocker locker(&endpoint);
        QDataStream endpointStateStream(&mStateUP->mEndpointNameToStateMap[endpointName], QIODevice::ReadOnly);
        endpointStateStream >> endpoint.state();
    }

    emit stateChanged(serializedState);
}

QString
QAbstractSyncManager::endpointIdentity(const QString& name)
{
    QString linkedIdentity = "unknown endpoint";

    auto const it = mEndpointNameToEndpointTUPMap.find(name);
    if (it != mEndpointNameToEndpointTUPMap.end())
    {
        auto const& endpoint = *it->second;
        linkedIdentity = endpoint.identity();
    }

    return linkedIdentity;
}

bool
QAbstractSyncManager::insertEndpoint(const QString& name, const QSyncEndpointData& data, QMutex* accessMutexP)
{   qDebug() << "insertEndpoint\n";

    if (mSyncing)
        return false;

    //    if (mEndpointNameToEndpointTUPMap.contains(name))    // C++20
    if (mEndpointNameToEndpointTUPMap.find(name) != mEndpointNameToEndpointTUPMap.end())
        return false;

    // FIX: Use plugin to find endpoint by type.

    if (data.driver == "dropbox")
        mEndpointNameToEndpointTUPMap[name] = std::make_unique_threaded<QDropboxSyncEndpoint>(this, name, data);
    else if (data.driver == "folder")
        mEndpointNameToEndpointTUPMap[name] = std::make_unique_threaded<QFolderSyncEndpoint>(this, name, data);
    else
        return false;
    Q_ASSERT(mEndpointNameToEndpointTUPMap[name]);

    mStateUP->mEndpointNameToDataMap[name] = data;
    mStateUP->mEndpointNameToStateMap[name] = QByteArray();

    auto& endpointTUP = mEndpointNameToEndpointTUPMap[name];
    auto& endpoint = *endpointTUP;
    endpoint.setAccessMutex(accessMutexP);

    connect(this, &QAbstractSyncManager::engageEndpoint, &endpoint, &QAbstractSyncEndpoint::engageEndpoint);
    connect(this, &QAbstractSyncManager::disengageEndpoint, &endpoint, &QAbstractSyncEndpoint::disengageEndpoint);
    connect(this, &QAbstractSyncManager::activateEndpoint, &endpoint, &QAbstractSyncEndpoint::activateEndpoint);
    connect(this, &QAbstractSyncManager::deactivateEndpoint, &endpoint, &QAbstractSyncEndpoint::deactivateEndpoint);
    connect(this, &QAbstractSyncManager::reportEndpoint, &endpoint, &QAbstractSyncEndpoint::reportEndpoint);
    connect(this, &QAbstractSyncManager::workEndpoint, &endpoint, &QAbstractSyncEndpoint::workEndpoint);
    connect(this, &QAbstractSyncManager::abortEndpoint, &endpoint, &QAbstractSyncEndpoint::abortEndpoint, Qt::DirectConnection);
    connect(&endpoint, &QAbstractSyncEndpoint::endpointEngaged, this, &QAbstractSyncManager::endpointEngaged);
    connect(&endpoint, &QAbstractSyncEndpoint::endpointDisengaged, this, &QAbstractSyncManager::endpointDisengaged);
    connect(&endpoint, &QAbstractSyncEndpoint::endpointReported, this, &QAbstractSyncManager::endpointReported);
    connect(&endpoint, &QAbstractSyncEndpoint::endpointWorked, this, &QAbstractSyncManager::endpointWorked);
    connect(&endpoint, &QAbstractSyncEndpoint::endpointStateChanged, this, &QAbstractSyncManager::endpointStateChanged);
    connect(&endpoint, &QAbstractSyncEndpoint::endpointStateMismatched, this, &QAbstractSyncManager::endpointStateMismatched);
    connect(&endpoint, &QAbstractSyncEndpoint::endpointError, this, &QAbstractSyncManager::endpointError, Qt::BlockingQueuedConnection);
    connect(&endpoint, &QAbstractSyncEndpoint::endpointQueueLengthChanged, this, &QAbstractSyncManager::endpointQueueLengthChanged);
//    connect(&endpoint, &QAbstractSyncEndpoint::browse, this, &QAbstractSyncManager::browse, Qt::BlockingQueuedConnection);
    connect(&endpoint, &QAbstractSyncEndpoint::openAuthorizeDialog, this, &QAbstractSyncManager::openAuthorizeDialog, Qt::BlockingQueuedConnection);
    connect(&endpoint, &QAbstractSyncEndpoint::closeAuthorizeDialog, this, &QAbstractSyncManager::closeAuthorizeDialog, Qt::BlockingQueuedConnection);
    connect(&endpoint, &QAbstractSyncEndpoint::openProgressDialog, this, &QAbstractSyncManager::openProgressDialog, Qt::BlockingQueuedConnection);
    connect(&endpoint, &QAbstractSyncEndpoint::closeProgressDialog, this, &QAbstractSyncManager::closeProgressDialog, Qt::BlockingQueuedConnection);

    mEndpointPToPollingMap[&endpoint] = false;
    mEndpointPToEngagedMap[&endpoint] = false;
    mEndpointPToReportedMap[&endpoint] = false;
    mEndpointPToWorkedMap[&endpoint] = false;

    return true;
}

bool
QAbstractSyncManager::removeEndpoint(const QString& name)
{   qDebug() << "removeEndpoint\n";
    if (mSyncing)
        return false;

//    if (!mEndpointNameToEndpointTUPMap.contains(name))    // C++20
    if (mEndpointNameToEndpointTUPMap.find(name) == mEndpointNameToEndpointTUPMap.end())
        return false;

    // FIX: any need to disengage first, or can we just rip it out?

    auto& endpointTUP = mEndpointNameToEndpointTUPMap[name];
    auto& endpoint = *endpointTUP;
    {
        QMutexLocker locker(&endpoint);
        disconnect(this, &QAbstractSyncManager::engageEndpoint, &endpoint, &QAbstractSyncEndpoint::engageEndpoint);
        disconnect(this, &QAbstractSyncManager::disengageEndpoint, &endpoint, &QAbstractSyncEndpoint::disengageEndpoint);
        disconnect(this, &QAbstractSyncManager::activateEndpoint, &endpoint, &QAbstractSyncEndpoint::activateEndpoint);
        disconnect(this, &QAbstractSyncManager::deactivateEndpoint, &endpoint, &QAbstractSyncEndpoint::deactivateEndpoint);
        disconnect(this, &QAbstractSyncManager::reportEndpoint, &endpoint, &QAbstractSyncEndpoint::reportEndpoint);
        disconnect(this, &QAbstractSyncManager::workEndpoint, &endpoint, &QAbstractSyncEndpoint::workEndpoint);
        disconnect(this, &QAbstractSyncManager::abortEndpoint, &endpoint, &QAbstractSyncEndpoint::abortEndpoint);
        disconnect(&endpoint, &QAbstractSyncEndpoint::endpointEngaged, this, &QAbstractSyncManager::endpointEngaged);
        disconnect(&endpoint, &QAbstractSyncEndpoint::endpointDisengaged, this, &QAbstractSyncManager::endpointDisengaged);
        disconnect(&endpoint, &QAbstractSyncEndpoint::endpointReported, this, &QAbstractSyncManager::endpointReported);
        disconnect(&endpoint, &QAbstractSyncEndpoint::endpointWorked, this, &QAbstractSyncManager::endpointWorked);
        disconnect(&endpoint, &QAbstractSyncEndpoint::endpointStateChanged, this, &QAbstractSyncManager::endpointStateChanged);
        disconnect(&endpoint, &QAbstractSyncEndpoint::endpointStateMismatched, this, &QAbstractSyncManager::endpointStateMismatched);
        disconnect(&endpoint, &QAbstractSyncEndpoint::endpointError, this, &QAbstractSyncManager::endpointError);
        disconnect(&endpoint, &QAbstractSyncEndpoint::endpointQueueLengthChanged, this, &QAbstractSyncManager::endpointQueueLengthChanged);
//        disconnect(&endpoint, &QAbstractSyncEndpoint::browse, this, &QAbstractSyncManager::browse);
        disconnect(&endpoint, &QAbstractSyncEndpoint::openAuthorizeDialog, this, &QAbstractSyncManager::openAuthorizeDialog);
        disconnect(&endpoint, &QAbstractSyncEndpoint::closeAuthorizeDialog, this, &QAbstractSyncManager::closeAuthorizeDialog);
        disconnect(&endpoint, &QAbstractSyncEndpoint::openProgressDialog, this, &QAbstractSyncManager::openProgressDialog);
        disconnect(&endpoint, &QAbstractSyncEndpoint::closeProgressDialog, this, &QAbstractSyncManager::closeProgressDialog);
    }

    (void) mStateUP->mEndpointNameToStateMap.remove(name);
    (void) mStateUP->mEndpointNameToDataMap.remove(name);

    (void) mEndpointPToWorkedMap.erase(&endpoint);
    (void) mEndpointPToReportedMap.erase(&endpoint);
    (void) mEndpointPToEngagedMap.erase(&endpoint);
    (void) mEndpointPToPollingMap.erase(&endpoint);

    (void) mEndpointNameToEndpointTUPMap.erase(name);

    return true;
}

bool
QAbstractSyncManager::removeEndpoints()
{
    // Don't use an iterator-based loop here, as we are deleting from the map.
    while (!mEndpointNameToEndpointTUPMap.empty())
    {
        auto const& [endpointName, endpointTUP] = *mEndpointNameToEndpointTUPMap.cbegin();
        if (!removeEndpoint(endpointName))
            return false;
    }
    return true;
}

bool
QAbstractSyncManager::engageEndpoints(bool wait, QSyncMode mode)
{   qDebug() << "engageEndpoints\n";
    if (mode == SyncModeDisabled)   // should use disengageEndpoints to disable endpoints
        return false;
    mMode = mode;

    mEndpointPToPollingMap.clear();
    mEndpointPToEngagedMap.clear();
    for (auto const& [endpointName, endpointTUP] : mEndpointNameToEndpointTUPMap)
    {
        auto& endpoint = *endpointTUP;
        mEndpointPToPollingMap[&endpoint] = true;
        mEndpointPToEngagedMap[&endpoint] = false;
    }

    // FIX: there's no accounting for false returns from this method
    emit engageEndpoint(mode);

    if (wait)
    {
        while (!allFalse(mEndpointPToPollingMap))
#ifdef Q_OS_IOS
            QCoreApplication::processEvents(QEventLoop::AllEvents);
#else
            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
#endif
        return allTrue(mEndpointPToEngagedMap);
    }

    return true;
}

void
QAbstractSyncManager::endpointEngaged(bool success)
{   qDebug() << "endpointEngaged\n";
    mEndpointPToPollingMap[qobject_cast<QAbstractSyncEndpoint*>(sender())] = false;
    mEndpointPToEngagedMap[qobject_cast<QAbstractSyncEndpoint*>(sender())] = success;
    if (!allFalse(mEndpointPToPollingMap))
        return;

    engagementCompleted();
}

void
QAbstractSyncManager::engagementCompleted()
{
    emit stateChanged(getSerializedState());
}

bool
QAbstractSyncManager::disengageEndpoints(bool wait)
{   qDebug() << "disengageEndpoints\n";
    mMode = SyncModeDisabled;

    mEndpointPToPollingMap.clear();
    mEndpointPToEngagedMap.clear();
    for (auto const& [endpointName, endpointTUP] : mEndpointNameToEndpointTUPMap)
    {
        auto& endpoint = *endpointTUP;
        mEndpointPToPollingMap[&endpoint] = true;
        mEndpointPToEngagedMap[&endpoint] = true;
    }

    emit disengageEndpoint();

    if (wait)
    {
        while (!allFalse(mEndpointPToPollingMap))
#ifdef Q_OS_IOS
            QCoreApplication::processEvents(QEventLoop::AllEvents);
#else
            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
#endif
        return allFalse(mEndpointPToEngagedMap);
    }

    return true;
}

void
QAbstractSyncManager::endpointDisengaged(bool success)
{   qDebug() << "endpointDisengaged\n";
    mEndpointPToPollingMap[qobject_cast<QAbstractSyncEndpoint*>(sender())] = false;
    mEndpointPToEngagedMap[qobject_cast<QAbstractSyncEndpoint*>(sender())] = !success;
    if (!allFalse(mEndpointPToPollingMap))
        return;

    disengagementCompleted();
}

void
QAbstractSyncManager::disengagementCompleted()
{
    emit stateChanged(getSerializedState());
}

void
QAbstractSyncManager::activateEndpoints()
{
    emit activateEndpoint();
}

void
QAbstractSyncManager::deactivateEndpoints()
{
    emit deactivateEndpoint();
}

bool
QAbstractSyncManager::startSync()
{   qDebug() << "startSync\n";
    if (mSyncing)
        return false;

    // Don't start sync if there are less than two endpoints.
    // FIX: count only endpoints with appropriate modes
    if (mEndpointNameToEndpointTUPMap.size() < 2)
        return false;

    // Don't start sync if any of the endpoints is disengaged.
    if (mEndpointPToEngagedMap.size() < mEndpointNameToEndpointTUPMap.size() || !allTrue(mEndpointPToEngagedMap))
        return false;

    // Be careful to save endpoint state before a cancel or abort tries to restore it.
    for (auto const& [endpointName, endpointTUP] : mEndpointNameToEndpointTUPMap)
    {
        auto& endpoint = *endpointTUP;
        endpoint.saveState();
    }

    mSyncing = true;
    mAborting = false;
    emit syncStarted();

    // FIX: These loops could be made more efficient.
    for (auto const& [endpointName, endpointTUP] : mEndpointNameToEndpointTUPMap)
    {
        auto& endpoint = *endpointTUP;
        for (auto const& path : endpoint.canonicalPaths())
        {
            for (auto const& [otherEndpointName, otherEndpointTUP] : mEndpointNameToEndpointTUPMap)
            {
                auto& otherEndpoint = *otherEndpointTUP;
                if (&otherEndpoint != &endpoint)
                    for (auto const& otherPath : otherEndpoint.canonicalPaths())
                    {
                        if ((otherPath + (otherPath.endsWith("/") ? "" : "/")).startsWith(path + (path.endsWith("/") ? "" : "/")))
                            throw QSyncException(std::string("sync endpoints overlap at: ") + path.toStdString());
                    }
            }
        }
    }

    // An abort signal may have arrived already.
    if (mAborting)
        return true;

    startSyncTimer();

    reportStarted();

    return true;
}

void
QAbstractSyncManager::reportStarted()
{   qDebug() << "reportStarted\n";
    mItemToEndpointPToItemMetadataUPMap.clear();
    mEndpointPToPollingMap.clear();
    mEndpointPToReportedMap.clear();
    for (auto const& [endpointName, endpointTUP] : mEndpointNameToEndpointTUPMap)
    {
        auto& endpoint = *endpointTUP;
        mEndpointPToPollingMap[&endpoint] = true;
        mEndpointPToReportedMap[&endpoint] = false;
    }

    emit reportEndpoint();
}

void
QAbstractSyncManager::endpointReported(bool success)
{   qDebug() << "endpointReported\n";
    // In case of an abort, we need to reset the polling flag.
    if (!mSyncing && !mAborting)
        return;

    mEndpointPToPollingMap[qobject_cast<QAbstractSyncEndpoint*>(sender())] = false;
    mEndpointPToReportedMap[qobject_cast<QAbstractSyncEndpoint*>(sender())] = success;
    if (!allFalse(mEndpointPToPollingMap))
        return;

    reportCompleted();
}

void
QAbstractSyncManager::reportCompleted()
{   qDebug() << "reportCompleted\n";
    if (!mSyncing)
        return;
    if (mAborting)
    {
        mAborting = false;
        return;
    }

    if (!allTrue(mEndpointPToReportedMap))
    {
        abortSync(QSyncException(std::string("one or more sync endpoints failed to report changed items")));
        return;
    }

    try
    {
        std::set<QString> conflictedItems;

        for (auto const& [endpointName, endpointTUP] : mEndpointNameToEndpointTUPMap)
        {
            auto& endpoint = *endpointTUP;
            QMutexLocker locker(&endpoint);
            for (auto const& item : endpoint.reportedItems())
            {
                auto& endpointPToItemMetadataUPMap = mItemToEndpointPToItemMetadataUPMap[item];
                if (!endpointPToItemMetadataUPMap.empty())
                    conflictedItems.insert(item);
                auto& metadataUP = endpointPToItemMetadataUPMap[&endpoint];
                metadataUP = metadataFactory();
                Q_ASSERT(metadataUP);
                endpoint.getItemMetadata(item, *metadataUP);
            }
        }

        if (conflictedItems.size() > 0)
        {
            QStringList itemList;
            QStringList useEndpoints;
            bool cancel = true;
            for (auto const& item : conflictedItems)
                itemList.append(item);
            emit handleConflicts(itemList, &useEndpoints, &cancel);
            if (cancel)
            {
                cancelSync();
                return;
            }
            for (auto const& item : conflictedItems)
            {
                auto& endpointPToItemMetadataUPMap = mItemToEndpointPToItemMetadataUPMap[item];
                // This loop uses iterators, because it can erase elements of the map.
                for (auto it = endpointPToItemMetadataUPMap.cbegin(); it != endpointPToItemMetadataUPMap.cend(); )
                {
                    auto const& [endpointP, metadataTUP] = *it;
                    if (!useEndpoints.contains(endpointP->name()))
                        it = endpointPToItemMetadataUPMap.erase(it);
                    else
                        ++it;
                }
                if (endpointPToItemMetadataUPMap.empty() ||
                        (endpointPToItemMetadataUPMap.size() >= 2 &&
                         resolveSyncConflict(item, endpointPToItemMetadataUPMap)))
                    mItemToEndpointPToItemMetadataUPMap.erase(item);
            }
        }

        for (auto const& [item, endpointPToItemMetadataUPMap] : mItemToEndpointPToItemMetadataUPMap)
        {
            // Unless there is an unresolved sync conflict, endpointPToItemMetadataUPMap should be a singleton map.
            if (endpointPToItemMetadataUPMap.size() != 1)
                throw QSyncException(std::string("failed to determine winning candidate for sync item: ") + item.toStdString());
            auto const& [endpointP, itemMetadataUP] = *endpointPToItemMetadataUPMap.cbegin();
            if (filterSyncItem(item, *itemMetadataUP, mItemToEndpointPToItemMetadataUPMap))
            {   // OK to process this sync item.
                auto& endpoint = *endpointP;
                if (!processSyncItem(endpoint, item, *itemMetadataUP))
                    throw QSyncException(std::string("failed to process sync item: ") + item.toStdString());
                if (mAborting)
                    break;
            }
        }
    }
    catch (const std::exception& e)
    {
        abortSync(e);
        return;
    }

    workStarted();
}

void
QAbstractSyncManager::workStarted()
{   qDebug() << "workStarted\n";
    mEndpointPToPollingMap.clear();
    mEndpointPToWorkedMap.clear();
    for (auto const& [endpointName, endpointTUP] : mEndpointNameToEndpointTUPMap)
    {
        auto& endpoint = *endpointTUP;
        mEndpointPToPollingMap[&endpoint] = true;
        mEndpointPToWorkedMap[&endpoint] = false;
    }

    emit workEndpoint();
}

void
QAbstractSyncManager::endpointWorked(bool success)
{   qDebug() << "endpointWorked\n";
    // In case of an abort, we need to reset the polling flag.
    if (!mSyncing && !mAborting)
        return;

    mEndpointPToPollingMap[qobject_cast<QAbstractSyncEndpoint*>(sender())] = false;
    mEndpointPToWorkedMap[qobject_cast<QAbstractSyncEndpoint*>(sender())] = success;
    if (!allFalse(mEndpointPToPollingMap))
        return;

    workCompleted();
}

void
QAbstractSyncManager::workCompleted()
{   qDebug() << "workCompleted\n";

    for (auto const& [endpointName, endpointTUP] : mEndpointNameToEndpointTUPMap)
    {
        auto& endpoint = *endpointTUP;
        endpoint.clearWorkItems();
    }

    if (mAborting)
    {
        mAborting = false;
        return;
    }
    if (!allTrue(mEndpointPToWorkedMap))
    {
        abortSync(QSyncException(std::string("one or more sync endpoints failed to process changed items")));
        return;
    }

    if (!mItemToEndpointPToItemMetadataUPMap.empty())
    {
        reportStarted();
        return;
    }

    mSyncTimer.stop();

    emit syncCompleted();
    mSyncing = false;

    emit stateChanged(getSerializedState());
}

void
QAbstractSyncManager::cancelSync()
{   qDebug() << "cancelSync\n";
    if (!mSyncing || mAborting)
        return;

    mSyncTimer.stop();

    mSyncing = false;
    mAborting = true;
    emit abortEndpoint();

    while (!allFalse(mEndpointPToPollingMap))
#ifdef Q_OS_IOS
        QCoreApplication::processEvents(QEventLoop::AllEvents);
#else
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
#endif

    for (auto const& [endpointName, endpointTUP] : mEndpointNameToEndpointTUPMap)
    {
        auto& endpoint = *endpointTUP;
        endpoint.restoreState();
        QDataStream endpointStateStream(&mStateUP->mEndpointNameToStateMap[endpointName], QIODevice::WriteOnly);
        endpointStateStream << endpoint.state();
    }

    emit syncCanceled();
}

void
QAbstractSyncManager::abortSync(const std::exception& e)
{   qDebug() << "abortSync\n";
    if (!mSyncing || mAborting)
        return;
    
    mSyncTimer.stop();
    
    mSyncing = false;
    mAborting = true;
    emit abortEndpoint();

    while (!allFalse(mEndpointPToPollingMap))
#ifdef Q_OS_IOS
        QCoreApplication::processEvents(QEventLoop::AllEvents);
#else
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
#endif

    for (auto const& [endpointName, endpointTUP] : mEndpointNameToEndpointTUPMap)
    {
        auto& endpoint = *endpointTUP;
        endpoint.restoreState();
        QDataStream endpointStateStream(&mStateUP->mEndpointNameToStateMap[endpointName], QIODevice::WriteOnly);
        endpointStateStream << endpoint.state();
    }

    emit syncAborted(e);
}

void
QAbstractSyncManager::setSyncTimeout(qint64 millisecs)
{
    mSyncTimeout = std::max<qint64>(millisecs, -1);
    if (mSyncTimer.isActive())
        startSyncTimer();
}

void
QAbstractSyncManager::startSyncTimer()
{
    if (mSyncTimeout >= 0)
    {
        mSyncTimer.setInterval(mSyncTimeout);
        mSyncTimer.start();
    }
    else
        mSyncTimer.stop();
}

void
QAbstractSyncManager::syncTimerExpired()
{   qDebug() << "syncTimerExpired\n";
    mSyncTimer.stop();

    bool cancel = true; // in case this signal is not connected
    emit syncTimedOut(&cancel);
    if (!cancel)
    {
        mSyncTimer.start();
        return;
    }

    abortSync(QSyncException("sync timed out"));
}

bool
QAbstractSyncManager::dateLessThan(const QDateTime& date1, const QDateTime& date2) const
{
    for (auto const& [endpointName, endpointTUP] : mEndpointNameToEndpointTUPMap)
    {
        auto& endpoint = *endpointTUP;
        if (!endpoint.dateLessThan(date1, date2))
            return false;
    }
    return true;
}

bool
QAbstractSyncManager::processSyncItem(QAbstractSyncEndpoint& fromEndpointP, const QString& item, const QAbstractSyncMetadata& metadata)
{
    auto const workItemSP = fromEndpointP.workItemFactory(item, &metadata);
    for (auto const& [endpointName, endpointTUP] : mEndpointNameToEndpointTUPMap)
    {
        auto& endpoint = *endpointTUP;
        endpoint.enqueueWorkItem(workItemSP);
    }
    return true;
}

void
QAbstractSyncManager::endpointStateChanged()
{   qDebug() << "endpointStateChanged\n";
    auto endpointP = dynamic_cast<QAbstractSyncEndpoint*>(sender());
    // This may be a null pointer if the client hasn't waited for endpoints to be fully disengaged before destroying this object.
    Q_ASSERT(endpointP);
    auto const& endpoint = *endpointP;
    auto const& endpointName = endpoint.name();
    QDataStream endpointStateStream(&mStateUP->mEndpointNameToStateMap[endpointName], QIODevice::WriteOnly);
    endpointStateStream << endpoint.state();
}

void
QAbstractSyncManager::endpointStateMismatched()
{   qDebug() << "endpointStateMismatched\n";
    auto const* endpointP = dynamic_cast<QAbstractSyncEndpoint*>(sender());
    // This may be a null pointer if the client hasn't waited for endpoints to be fully disengaged before destroying this object.
    Q_ASSERT(endpointP);
    auto const& endpoint = *endpointP;
    auto const& endpointName = endpoint.name();
    abortSync(QSyncException(std::string("full resync required due to endpoint: ") + endpointName.toStdString()));

    emit syncReset();
}

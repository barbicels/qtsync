#include <QtCore/QDebug>
#include <QtCore/QDirIterator>
#include <QtCore/QSaveFile>
#include "QDropboxHash.h"
#ifdef TARGET_OS_IOS
#include "NSFileManagerShim.h"
#else
    #ifdef Q_OS_ANDROID
    #include "AFileManagerShim.h"
    #endif
#endif
#include "qfoldersyncendpoint.h"

QFolderSyncEndpoint::QFolderSyncEndpoint(const QString& name, const QSyncEndpointData& data, QObject* parent)
    : QAbstractSyncEndpoint(name, FOLDER_ENDPOINT_LOCALITY, parent),
      mFolder(QSync::getSandboxAbsolutePath(data.parameters["folder"]))
{
    mStateUP = std::make_unique<QFolderSyncEndpointState>();
    Q_ASSERT(mStateUP);
}

void
QFolderSyncEndpoint::QFolderSyncMetadata::serializeTo(QDataStream& stream) const
{
    stream << mLastModified
           << mCaseSensitivePath;
}

void
QFolderSyncEndpoint::QFolderSyncMetadata::serializeFrom(QDataStream& stream)
{
    stream >> mLastModified
           >> mCaseSensitivePath;
}

QFolderSyncEndpoint::QFolderSyncEndpointState::QFolderSyncEndpointState() : QAbstractSyncEndpointState()
{

}

QFolderSyncEndpoint::QFolderSyncEndpointState::QFolderSyncEndpointState(const QFolderSyncEndpointState& other) : QAbstractSyncEndpointState(other)
{
    mDateTimeCursor = other.mDateTimeCursor;
    mItemToItemMetadataMap = other.mItemToItemMetadataMap;
}

std::unique_ptr<QAbstractSyncEndpoint::QAbstractSyncEndpointState>
QFolderSyncEndpoint::QFolderSyncEndpointState::clone() const
{
    return std::make_unique<QFolderSyncEndpointState>(*this);
}

QDateTime
QFolderSyncEndpoint::QFolderSyncEndpointState::getDateTimeCursor() const
{
    return mDateTimeCursor;
}

void
QFolderSyncEndpoint::QFolderSyncEndpointState::setDateTimeCursor(const QDateTime& dateTime)
{
    mDateTimeCursor = dateTime;
}

QMap<QString, QFolderSyncEndpoint::QFolderSyncMetadata>&
QFolderSyncEndpoint::QFolderSyncEndpointState::getItemToItemMetadataMap()
{
    return mItemToItemMetadataMap;
}

void
QFolderSyncEndpoint::QFolderSyncEndpointState::serializeTo(QDataStream& stream) const
{
    QAbstractSyncEndpointState::serializeTo(stream);
    stream.setVersion(QDataStream::Qt_5_12);
    stream << (qint32) Q_FOLDER_SYNC_ENDPOINT_STATE_VERSION_1;
    // FIX: stream mFolder
    // Q_FOLDER_SYNC_ENDPOINT_STATE_VERSION_1
    stream << mDateTimeCursor
           << mItemToItemMetadataMap;
}

void
QFolderSyncEndpoint::QFolderSyncEndpointState::serializeFrom(QDataStream& stream)
{
    QAbstractSyncEndpointState::serializeFrom(stream);
    mDateTimeCursor = QDateTime();
    mItemToItemMetadataMap.clear();
    stream.setVersion(QDataStream::Qt_5_12);
    qint32 version;
    stream >> version;
    switch (version)
    {
    case 0: // null
        return;
    case Q_FOLDER_SYNC_ENDPOINT_STATE_VERSION_1:
        break;
    default:
        throw QSyncException(std::string("unknown folder sync endpoint state version: ") + std::to_string(version));
    }
    // FIX: unstream mFolder and check / throw error if mismatch
    if (version >= Q_FOLDER_SYNC_ENDPOINT_STATE_VERSION_1)
        stream >> mDateTimeCursor
               >> mItemToItemMetadataMap;
}

QString
QFolderSyncEndpoint::identity() const
{
    return QString("Folder: %1").arg(mFolder.absolutePath());
}

bool
QFolderSyncEndpoint::dateLessThan(const QDateTime& date1, const QDateTime& date2) const
{
    // All Qt file-system implementations promise millisecond precision.
    return date1.toMSecsSinceEpoch() < date2.toMSecsSinceEpoch();
}

bool
QFolderSyncEndpoint::report(const bool& abort)
{   qDebug() << "folder: report\n";
    QMutexLocker locker(accessMutex());
    // If the folder is missing or unreadable for any reason, don't proceed!
    // An iterator giving no items will report that all items were deleted.
    // Don't trust mFolder.isReadable() to be up to date.
    if (!QFileInfo(mFolder.absolutePath()).isReadable())
        throw QSyncException(std::string("failed to fetch list of changed items from folder"));
    mReportedItems = QStringList();
    mDeletedItemToCaseSensitivePathMap.clear();
    auto& endpointState = *dynamic_cast<QFolderSyncEndpointState*>(mStateUP.get());
    auto& itemToItemMetadataMap = endpointState.getItemToItemMetadataMap();
    auto existingItems = itemToItemMetadataMap.keys();
    QDirIterator it(mFolder, QDirIterator::Subdirectories);
    while (it.next() != "" && !abort)
    {
        auto const info = it.fileInfo();
        if (info.exists())
        {
            auto const filename = info.fileName();
            if (filename != "." && filename != "..")
            {
                auto const caseSensitivePath = QDir::cleanPath(mFolder.relativeFilePath(info.filePath()));
                auto const item = caseSensitivePath.toLower();
                // Resolve any case-duplicate names by collating order, not first occurrence.
                // NOTE: QDirIterator explicitly does not guarantee sorting, and some implementations
                // (e.g., Android) have been observed to return different orders at different times.
                if (mReportedItems.contains(item))
                {
                    if (caseSensitivePath < itemToItemMetadataMap[item].mCaseSensitivePath)
                    {
                        itemToItemMetadataMap[item].mLastModified = info.lastModified();
                        itemToItemMetadataMap[item].mCaseSensitivePath = caseSensitivePath;
                    }
                }
                else
                {
                    existingItems.removeOne(item);
                    // Report item if it is new, has a different last-modified date, or is case-renamed.
                    if (!itemToItemMetadataMap.contains(item)
                            || (!info.isDir() && info.lastModified() != itemToItemMetadataMap[item].mLastModified)
                            || info.fileName() != itemToItemMetadataMap[item].mCaseSensitivePath.right(info.fileName().length()))
                    {
                        itemToItemMetadataMap[item].mLastModified = info.lastModified();
                        itemToItemMetadataMap[item].mCaseSensitivePath = caseSensitivePath;
                        mReportedItems << item;
                    }
                }
            }
        }
    }
    if (abort)
        return false;
    for (auto const& item : existingItems)
    {   // Record the case-sensitive path of this deleted item (required for inclusion in file metadata).
        mDeletedItemToCaseSensitivePathMap[item] = itemToItemMetadataMap[item].mCaseSensitivePath;
        itemToItemMetadataMap.remove(item);
    }
    mReportedItems << existingItems;
    endpointState.setDateTimeCursor(QDateTime::currentDateTime());
    return true;
}

void
QFolderSyncEndpoint::getFileMetadata(const QString& item, QFileSyncManager::QFileSyncMetadata& fileMetadata) const
{
    QMutexLocker locker(accessMutex());
    //    if (!mDeletedItemToCaseSensitivePathMap.contains(item))    // C++20
    if (mDeletedItemToCaseSensitivePathMap.find(item) == mDeletedItemToCaseSensitivePathMap.end())
    {
        auto& endpointState = *dynamic_cast<QFolderSyncEndpointState*>(mStateUP.get());
        auto& itemToItemMetadataMap = endpointState.getItemToItemMetadataMap();
        const QFileInfo info(mFolder, itemToItemMetadataMap[item].mCaseSensitivePath);
        // If a file or folder has disappeared since the report, provideItem will ignore it and return false.
        if (info.exists() && !info.isDir())
        {
            QString filePath = mFolder.absoluteFilePath(info.canonicalFilePath());
            fileMetadata.mLastModified = info.lastModified();
            fileMetadata.mVersion = QString();  // not implemented
            fileMetadata.mDropboxHashGetter = [filePath] { return QDropboxHash::hash(filePath); };
            fileMetadata.mSize = info.size();
        }
        else
            fileMetadata.mIsFolder = true;
        fileMetadata.mCaseSensitivePath = itemToItemMetadataMap[item].mCaseSensitivePath;
    }
    else
    {
        fileMetadata.mIsDeleted = true;
        fileMetadata.mCaseSensitivePath = mDeletedItemToCaseSensitivePathMap.at(item);
    }
}

void
QFolderSyncEndpoint::getItemMetadata(const QString& item, QAbstractSyncMetadata& metadata)
{
    auto* const fileMetadataP = dynamic_cast<QFileSyncManager::QFileSyncMetadata*>(&metadata);
    if (fileMetadataP != nullptr)
    {
        auto& fileMetadata = *fileMetadataP;
        getFileMetadata(item, fileMetadata);
    }
    else
        throw QSyncException(std::string("metadata class unknown to folder sync endpoint: ") + item.toStdString());
}

bool
QFolderSyncEndpoint::provideItem(QAbstractSyncEndpointWorkItem& workItem, const bool& abort)
{   qDebug() << "folder: provideItem (" + workItem.item + ")\n";
    Q_UNUSED(abort)
    QMutexLocker locker(accessMutex());
    auto const* const fileMetadataP = dynamic_cast<const QFileSyncManager::QFileSyncMetadata*>(workItem.metadataP);
    if (fileMetadataP != nullptr)
    {
        auto const& fileMetadata = *fileMetadataP;
        auto const absolutePath = mFolder.absoluteFilePath(fileMetadata.mCaseSensitivePath);
        if (fileMetadata.mIsFolder)
        {   // folder
            auto const folderInfoUP = std::make_unique<QFileInfo>(absolutePath);
            Q_ASSERT(folderInfoUP);
            if (!folderInfoUP->exists() || !folderInfoUP->isDir())
                return false;
        }
        else if (fileMetadata.mIsDeleted)
            ;   // deleted: nothing to do
        else
        {   // file
            auto fileUP = std::make_unique<QFile>(absolutePath);
            Q_ASSERT(fileUP);
            if (!fileUP->exists())
                return false;
            workItem.deviceUP = std::move(fileUP);
        }
        return true;
    }
    else
        throw QSyncException(std::string("metadata class unknown to folder sync endpoint: ") + workItem.item.toStdString());
}

bool
QFolderSyncEndpoint::consumeItem(QAbstractSyncEndpointWorkItem& workItem, const bool& abort)
{   qDebug() << "folder: consumeItem (" + workItem.item + ")\n";
    Q_UNUSED(abort)
    QMutexLocker locker(accessMutex());
    auto& endpointState = *dynamic_cast<QFolderSyncEndpointState*>(mStateUP.get());
    auto& itemToItemMetadataMap = endpointState.getItemToItemMetadataMap();
    auto const* const fileMetadataP = dynamic_cast<const QFileSyncManager::QFileSyncMetadata*>(workItem.metadataP);
    if (fileMetadataP != nullptr)
    {
        auto const& fileMetadata = *fileMetadataP;
        // NOTE: Some endpoints (notably Dropbox) do not promise case-correctness of all path components, only the last.
        // Reference: https://www.dropbox.com/developers/documentation/http/documentation#case-insensitivity
        // We attempt to cover for this by walking up the folder structure to find a presumed match in the form of a
        // case-corrected absolutePath.  This is OK, as long as folder adds/changes are consumed before their contents.
        // There may be multiple levels of case corrections to make, hence the while loop.
        auto absolutePath = mFolder.absoluteFilePath(fileMetadata.mCaseSensitivePath);
        while (!QFileInfo(absolutePath).exists())
        {
            QDir matchDir(QFileInfo(absolutePath).path());
            QString dirName;
            while (!matchDir.exists())
            {
                dirName = matchDir.dirName();
                matchDir = QDir(QFileInfo(matchDir.path()).path());
            }
            if (dirName.isEmpty())  // item's enclosing folder exists
                break;
            const auto matchDirNames = matchDir.entryList(QStringList(dirName), QDir::Dirs, QDir::Name);
            if (matchDirNames.isEmpty())    // no case-insensitive folder match found
                break;
            // try again with case-corrected absolutePath
            absolutePath = absolutePath.replace(matchDir.path().length() + 1, dirName.length(), matchDirNames[0]);
        }
        const QFileInfo info(absolutePath);
        if (fileMetadata.mIsFolder)
        {   // folder
            QDir parentDir(info.path());
            if (info.exists())
            {   // Handle possible case change in a case-insensitive file system.
                if (!info.isDir())
                    throw QSyncException(QString("cannot sync folder due to file conflict: " + workItem.item).toStdString());
                if (info.fileName() != info.canonicalFilePath().right(info.fileName().length()) &&
                        !parentDir.rename(info.canonicalFilePath().right(info.fileName().length()), info.fileName()))
                    throw QSyncException(std::string("failed to rename folder: ") + workItem.item.toStdString());
            }
            else
            {   // Check for another folder that matches case-insensitively, and rename it.
                // This can happen on case-sensitive file systems, where info.exists() would return false,
                // and this will occur if the providing endpoint is reporting a case-renamed folder.
                // QDir::entryList is case-insensitive by default.
                auto otherDirNames = parentDir.entryList(QStringList(info.fileName()), QDir::Dirs, QDir::Name);
                otherDirNames.removeAll(info.fileName());
                if (otherDirNames.isEmpty())
                {
                    if (!mFolder.mkpath(fileMetadata.mCaseSensitivePath))
                        throw QSyncException(std::string("failed to create folder: ") + workItem.item.toStdString());
                }
                else
                {
                    auto const& otherDirName = otherDirNames[0];
                    qDebug() << "folder: consumeItem renaming case-duplicate folder (" + parentDir.absoluteFilePath(otherDirName) + ")\n";
                    if (!parentDir.rename(otherDirName, info.fileName()))
                        throw QSyncException(std::string("failed to rename folder: ") + workItem.item.toStdString());
                }
            }
            itemToItemMetadataMap[workItem.item].mLastModified = QFileInfo(absolutePath).lastModified();
            itemToItemMetadataMap[workItem.item].mCaseSensitivePath = fileMetadata.mCaseSensitivePath;
        }
        else if (fileMetadata.mIsDeleted)
        {   // deleted
            if (info.exists())
            {
                if (info.isDir())
                {
                    QDir folder(absolutePath);
                    if (!folder.removeRecursively())
                        throw QSyncException(std::string("failed to delete folder: ") + workItem.item.toStdString());
                }
                else
                {
                    QFile file(absolutePath);
                    if (!file.remove())
                        throw QSyncException(std::string("failed to delete file: ") + workItem.item.toStdString());
                }
            }
            itemToItemMetadataMap.remove(workItem.item);
        }
        else
        {   // file
            if (!(workItem.deviceUP && workItem.deviceUP->isReadable()))
                return false;
            // Device data is available, so write to file.
            QDir parentDir(info.path());
            if (info.exists())
            {   // Handle possible case change in a case-insensitive file system.
                if (info.isDir())
                    throw QSyncException(std::string("cannot sync file due to folder conflict: ") + workItem.item.toStdString());
                if (info.fileName() != info.canonicalFilePath().right(info.fileName().length()) &&
                        !parentDir.rename(info.canonicalFilePath().right(info.fileName().length()), info.fileName()))
                    throw QSyncException(std::string("failed to rename file: ") + workItem.item.toStdString());
            }
            else if (!parentDir.mkpath("."))
                throw QSyncException(std::string("failed to create parent folder: ") + workItem.item.toStdString());
            {   // First, use QSaveFile to perform a "safe save".
                auto const saveFileUP = std::make_unique<QSaveFile>(absolutePath);
                Q_ASSERT(saveFileUP);
                saveFileUP->setDirectWriteFallback(true);
                if (!saveFileUP->open(QFile::WriteOnly))
                    throw QSyncException(std::string("failed to open file for write: ") + workItem.item.toStdString());
                auto const bytesAvailable = workItem.deviceUP->bytesAvailable();
                if (saveFileUP->write(workItem.deviceUP->readAll()) != bytesAvailable)
                    throw QSyncException(std::string("failed to write to file: ") + workItem.item.toStdString());
                if (!saveFileUP->commit())
                    throw QSyncException(std::string("failed to save file: ") + workItem.item.toStdString());
            }
            {   // Second, set the provided last-modification date.
                // QFileDevice::setFileTime is stubbed out in the iOS and Android platform plugins' QAbstractFileEngine, so we have to use shim code.
#ifdef Q_OS_IOS
                if (!NSFileManagerShim_setFileTime(absolutePath,
#else
#ifdef Q_OS_ANDROID
                if (!AFileManagerShim_setFileTime(absolutePath,
#else
                auto const fileUP = std::make_unique<QFile>(absolutePath);
                Q_ASSERT(fileUP);
                if (!fileUP->open(QFile::Append))
                    throw QSyncException(std::string("failed to open file to set last-modified date: ") + workItem.item.toStdString());
                if (!fileUP->setFileTime(
#endif
#endif
                            fileMetadata.mLastModified, QFile::FileModificationTime))
                    throw QSyncException(std::string("failed to set last-modified date for file: ") + workItem.item.toStdString());
            }
            itemToItemMetadataMap[workItem.item].mLastModified = QFileInfo(absolutePath).lastModified();
            itemToItemMetadataMap[workItem.item].mCaseSensitivePath = fileMetadata.mCaseSensitivePath;
            {   // Finally, check for other files that match case-insensitively, and delete them.
                // This can happen on case-sensitive file systems, where info.exists() would return false,
                // and this will occur if the providing endpoint is reporting a case-renamed file.
                // QDir::entryList is case-insensitive by default.
                auto otherFileNames = parentDir.entryList(QStringList(info.fileName()), QDir::Files, QDir::Name);
                otherFileNames.removeAll(info.fileName());
                for (auto const& otherFileName : otherFileNames)
                {
                    QFile otherFile(parentDir.absoluteFilePath(otherFileName));
                    qDebug() << "folder: consumeItem deleting case-duplicate file (" + parentDir.absoluteFilePath(otherFileName) + ")\n";
                    if (!otherFile.remove())
                        throw QSyncException(std::string("failed to delete case-duplicate file: ") + workItem.item.toStdString());
                }
            }
        }
        return true;
    }
    else
        throw QSyncException(std::string("metadata class unknown to folder sync endpoint: ") + workItem.item.toStdString());
}

QStringList
QFolderSyncEndpoint::getCanonicalPaths()
{   qDebug() << "folder: getCanonicalPaths\n";
    QStringList canonicalPaths;

    canonicalPaths += QString("folder:/")
        + "/" + mFolder.absolutePath().replace(QDir::separator(), '/');

    return canonicalPaths;
}

bool
QFolderSyncEndpoint::isReady()
{
    return true;
}

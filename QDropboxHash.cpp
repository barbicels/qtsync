#include <QtCore/QCryptographicHash>
#include <QtCore/QFile>
#include "QDropboxHash.h"

QString
QDropboxHash::hash(const QString& filename)
{
    QByteArray assembledBlockHashes;
    QFile file(filename);
    file.open(QFile::ReadOnly);
    Q_ASSERT(file.isOpen());
    while (file.bytesAvailable() > 0)
    {
        constexpr int maximumBlockSize = 4 * 1024 * 1024;
        QByteArray block = file.read(maximumBlockSize);
        assembledBlockHashes += QCryptographicHash::hash(block, QCryptographicHash::Sha256);
    }
    file.close();
    return QCryptographicHash::hash(assembledBlockHashes, QCryptographicHash::Sha256).toHex();
}

#ifndef QDROPBOXHASH_H
#define QDROPBOXHASH_H

#include <QtCore/QString>

class QDropboxHash
{
public:
    // Return the Dropbox-style SHA-256 file hash as a decimal string.
    static QString hash(const QString& filename);
};

#endif // QDROPBOXHASH_H

QT -= gui
QT += network
# FIX: This module dependency should be moved to plugins that require it.
QT += httpserver

TEMPLATE = lib
DEFINES += QTSYNC_LIBRARY

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += \
    $$PWD/../dropboxQt/src

android:LIBS += -L$$PWD/lib/android
ios:LIBS += -L$$PWD/lib/ios
macx:LIBS += -L$$PWD/lib/macx
unix:!macx:linux-g++-32:LIBS += -L$$PWD/lib/unix
unix:!macx:linux-g++:LIBS += -L$$PWD/lib/unix-64
win32:LIBS += -L$$PWD/lib/win32
macx {
    versionAtLeast(QT_VERSION, 6.5) {
        QMAKE_MACOSX_DEPLOYMENT_TARGET = 11
    } else {
        QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.13
    }
    QMAKE_CXXFLAGS += -stdlib=libc++
    QMAKE_LFLAGS += -stdlib=libc++
    # BUG: Qt <5.12.8/5.15.0 with Xcode 11.4+ fails to add SDK version
    # to LC_VERSION_MIN_MACOSX section in generated .dylib.
    # https://codereview.qt-project.org/c/qt/qtbase/+/295358
    QMAKE_LFLAGS += -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk
}

!android:LIBS += \
    -ldropboxQt
# This multi-ABI library integration works for Qt 5.14 and later.
android:LIBS += \
    -ldropboxQt_$${QT_ARCH}

ios:OBJECTIVE_SOURCES += \
    NSBackgroundTaskShim.mm \
    NSFileManagerShim.mm
android:SOURCES += \
    AFileManagerShim.cpp
SOURCES += \
    QDropboxHash.cpp \
    qabstractsyncendpoint.cpp \
    qabstractsyncmanager.cpp \
    qabstractsyncmetadata.cpp \
    qdropboxsyncendpoint.cpp \
    qfilesyncmanager.cpp \
    qfoldersyncendpoint.cpp

ios:OBJECTIVE_HEADERS += \
    NSBackgroundTaskShim.h \
    NSFileManagerShim.h
android:HEADERS += \
    AFileManagerShim.h
HEADERS += \
    QDropboxHash.h \
    QThreadedUniquePtr.h \
    qabstractsyncendpoint.h \
    qabstractsyncmetadata.h \
    qdropboxsyncendpoint.h \
    qfilesyncmanager.h \
    qfoldersyncendpoint.h \
    qsync.h \
    qtsync_global.h \
    qabstractsyncmanager.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target

#include <QtCore/QDebug>
#include "qfilesyncmanager.h"

QFileSyncManager::QFileSyncManager(QObject* parent)
    : QAbstractSyncManager(parent)
{
    mStateUP = std::make_unique<QFileSyncManagerState>();
    Q_ASSERT(mStateUP);
}

QFileSyncManager::QFileSyncMetadata::QFileSyncMetadata()
    : QAbstractSyncMetadata(),
      /*mPath(path),*/
      mIsFolder(false),
      mIsDeleted(false),
      mLastModified(),
      mVersion(),
      mDropboxHashGetter(),
      mSize(-1),
      mCaseSensitivePath()
{

}

//void
//QFileSyncManager::QFileSyncMetadata::serializeTo(QDataStream& stream) const
//{
//    stream << mIsFolder
//           << mIsDeleted
//           << mLastModified
//           << mVersion
//           << mDropboxHash
//           << mSize
//           << mCaseSensitivePath;
//}

//void
//QFileSyncManager::QFileSyncMetadata::serializeFrom(QDataStream& stream)
//{
//    stream >> mIsFolder
//           >> mIsDeleted
//           >> mLastModified
//           >> mVersion
//           >> mDropboxHash
//           >> mSize
//           >> mCaseSensitivePath;
//}

std::unique_ptr<QAbstractSyncManager::QAbstractSyncManagerState> QFileSyncManager::QFileSyncManagerState::clone() const
{
    return std::make_unique<QFileSyncManagerState>(*this);
}

void
QFileSyncManager::QFileSyncManagerState::serializeTo(QDataStream& stream) const
{
    QAbstractSyncManagerState::serializeTo(stream);
    stream.setVersion(QDataStream::Qt_5_12);
    stream << (qint32) Q_FILE_SYNC_MANAGER_STATE_VERSION_1;
}

void
QFileSyncManager::QFileSyncManagerState::serializeFrom(QDataStream& stream)
{
    QAbstractSyncManagerState::serializeFrom(stream);
    stream.setVersion(QDataStream::Qt_5_12);
    qint32 version;
    stream >> version;
    switch (version)
    {
    case 0: // null
        return;
    case Q_FILE_SYNC_MANAGER_STATE_VERSION_1:
        break;
    default:
        throw QSyncException(std::string("unknown file sync manager state version: ") + std::to_string(version));
    }
}

std::unique_ptr<QAbstractSyncMetadata>
QFileSyncManager::metadataFactory()
{
    return std::make_unique<QFileSyncMetadata>();
}

const QFileSyncManager::QFileSyncMetadata&
QFileSyncManager::castAsFileMetadata(const QString& item, const QAbstractSyncMetadata& metadata) const
{
    try
    {
        return dynamic_cast<const QFileSyncMetadata&>(metadata);
    }
    catch (const std::bad_cast&)
    {
        throw QSyncException(std::string("metadata class unknown to file sync manager: ") + item.toStdString());
    }
}

bool
QFileSyncManager::resolveSyncConflict(const QString& item, std::map<QAbstractSyncEndpoint*, std::unique_ptr<QAbstractSyncMetadata>>& endpointPToItemMetadataUPMap)
{
    auto foundFile = false;
    auto foundFolder = false;
    auto foundDeleted = false;
    const QFileSyncMetadata* latestMetadataP;
    QDateTime latestModified;
    QString latestVersion;
    auto candidates = endpointPToItemMetadataUPMap.size();

    for (auto const& [endpointP, itemMetadataTUP] : endpointPToItemMetadataUPMap)
    {
        auto const& itemMetadata = *itemMetadataTUP;
        auto const& fileMetadata = castAsFileMetadata(item, itemMetadata);
        if (fileMetadata.mIsFolder)
            foundFolder = true;
        else if (fileMetadata.mIsDeleted)
            foundDeleted = true;
        else
        {
            foundFile = true;
            if (dateLessThan(latestModified, fileMetadata.mLastModified))
            {
                latestMetadataP = &fileMetadata;
                latestModified = fileMetadata.mLastModified;
            }
            latestVersion = std::max(fileMetadata.mVersion, latestVersion);
        }
    }

    if (foundFile && foundFolder) // need to implement user interaction for resolution failures
        throw QSyncException(std::string("unresolved file sync conflict (file vs. folder): ") + item.toStdString());
    if (foundDeleted && (foundFile || foundFolder))
//        throw QSyncException(std::string("unresolved file sync conflict (deletion vs. file/folder): ") + item.toStdString());
    {
        qWarning() << ("warning: restoring deleted item: " + item);
        // Erase any deleted-metadata elements from the map.
        // This loop uses iterators, because it can erase elements of the map.
        for (auto it = endpointPToItemMetadataUPMap.cbegin(); it != endpointPToItemMetadataUPMap.cend(); )
        {
            auto const& [endpointP, itemMetadataTUP] = *it;
            auto const& itemMetadata = *itemMetadataTUP;
            auto const& fileMetadata = castAsFileMetadata(item, itemMetadata);
            if (fileMetadata.mIsDeleted)
                it = endpointPToItemMetadataUPMap.erase(it);
            else
                ++it;
        }
    }

    if (foundFile && endpointPToItemMetadataUPMap.size() > 1)
    {   // Look for discrepancies in hash, last-modified time, version, and size.
        QString latestDropboxHash;
        // This loop uses iterators, because it can erase elements of the map.
        for (auto it = endpointPToItemMetadataUPMap.cbegin(); it != endpointPToItemMetadataUPMap.cend(); )
        {
            auto const& [endpointP, itemMetadataTUP] = *it;
            auto const& itemMetadata = *itemMetadataTUP;
            auto const& fileMetadata = castAsFileMetadata(item, itemMetadata);
            // dateLessThan allows for variable levels of precision among endpoints.
            if (dateLessThan(fileMetadata.mLastModified, latestModified)
//                    || (!fileMetadata.mVersion.isNull() && fileMetadata.mVersion < latestVersion) // not implemented - watch out for conflicting mod-date/rev ordering!
                    )
            {   // Before removing this candidate, check for a Dropbox hash match.
                // The hash getter can be slow, so only call it when comparison is necessary.
                if (latestDropboxHash.isEmpty())
                    latestDropboxHash = latestMetadataP->mDropboxHashGetter();
                if (fileMetadata.mDropboxHashGetter() != latestDropboxHash)
                    it = endpointPToItemMetadataUPMap.erase(it);
                else
                    ++it;
            }
            else
                ++it;
        }
        if (endpointPToItemMetadataUPMap.size() > 1)
        {
            qint64 size = -1;
            for (auto const& [endpointP, itemMetadataTUP] : endpointPToItemMetadataUPMap)
            {
                auto const& itemMetadata = *itemMetadataTUP;
                auto const& fileMetadata = castAsFileMetadata(item, itemMetadata);
                if (size < 0)
                    size = fileMetadata.mSize;
                else if (fileMetadata.mSize != size) // FIX: need to implement user interaction for resolution failures
                    throw QSyncException(std::string("unresolved file sync conflict (files are different sizes): ") + item.toStdString());
            }
        }
    }

    // If all candidates seem identical, there is nothing to replicate.
    if (endpointPToItemMetadataUPMap.size() == candidates)
    {
        endpointPToItemMetadataUPMap.clear();
        return true;
    }

    // Choose the most local candidate to replicate to the others.
    while (endpointPToItemMetadataUPMap.size() > 1)
        endpointPToItemMetadataUPMap.erase(endpointPToItemMetadataUPMap.cend());
    return false;
}

bool
QFileSyncManager::filterSyncItem(const QString& item, const QAbstractSyncMetadata& metadata, const std::map<QString, std::map<QAbstractSyncEndpoint*, std::unique_ptr<QAbstractSyncMetadata>>>& itemToEndpointPToItemMetadataUPMap)
{
    auto const& fileMetadata = castAsFileMetadata(item, metadata);
    // Don't delete a folder if there is any item inside it that warrants an action other than deletion.
    // This is important in cases where one endpoint wants to delete a folder while another wants to create a file inside it.
    if (fileMetadata.mIsDeleted)
    {
        for (auto const& [otherItem, endpointPToItemMetadataUPMap] : itemToEndpointPToItemMetadataUPMap)
        {
            if (otherItem.startsWith(item + "/"))
            {
                for (auto const& [endpointP, otherItemMetadataUP] : endpointPToItemMetadataUPMap)
                {
                    auto const& otherFileMetadata = castAsFileMetadata(otherItem, *otherItemMetadataUP);
                    if (!otherFileMetadata.mIsDeleted)
                        return false;
                }
            }
        }
    }
    return true;
}

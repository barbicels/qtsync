#ifndef QSYNC_H
#define QSYNC_H

#include <exception>
#include <iostream>
#include <memory>
#include <string>
#include <QtCore/QDataStream>
#include <QtCore/QDir>
#include <QtCore/QMap>
#include <QtCore/QRegularExpression>
#include <QtCore/QStandardPaths>
#include "qtsync_global.h"

enum QSyncMode {
    SyncModeDisabled = 0,
    SyncModeInOnly = 1,
    SyncModeOutOnly = 2,
    SyncModeInOut = 3,
};

struct QTSYNC_EXPORT QSyncEndpointData
{
    QString driver;
    QMap<QString, QString> parameters;
    bool operator==(const QSyncEndpointData& other) const { return driver == other.driver && parameters == other.parameters; }
    friend QDataStream& operator<<(QDataStream& stream, const QSyncEndpointData& data) { stream << data.driver << data.parameters; return stream; }
    friend QDataStream& operator>>(QDataStream& stream, QSyncEndpointData& data) {stream >> data.driver >> data.parameters; return stream; }
};

class QTSYNC_EXPORT QSyncException : public std::exception
{
public:
//    explicit QSyncException(const std::string& message)
//        : QSyncException(message, 0) { }

//    explicit QSyncException(const std::string& message, int code)
//        : QSyncException(message, code, "") { }

//    explicit QSyncException(const std::string& message, int code, const std::string summary)
//        : QSyncException(message, code, summary, nullptr) { }

    explicit QSyncException(const std::string& message, int code = 0, const std::string summary = "", const std::exception* innerException = nullptr)
        : m_msg(message), m_status_code(code), m_error_summary(summary) { build(innerException ? innerException->what() : ""); }

    virtual const char* what() const throw () { return m_what.c_str(); }

    virtual int statusCode() const throw() { return m_status_code; }

    ///Polymorphic exception idiom
    virtual void raise() { throw *this; }

protected:
    void build(std::string err)
    {
        m_what = m_msg;
        if (!m_error_summary.empty())
            m_what += "\nSUMMARY:" + m_error_summary;
        if (!err.empty())
            m_what += "\nERROR:\n" + err;
    }

protected:
    std::string m_msg;
    int m_status_code;
    std::string m_error_summary;
    std::string m_what;
};

class QTSYNC_EXPORT QSync
{
public:
    #ifdef Q_OS_IOS
    // This could also apply to Android, as a sandboxed OS, but it's not necessary
    // because Android's sandbox is named after the bundle identifier and doesn't
    // move around between installations as iOS's does.
    // Exempting Android from this #define is kinder to past beta-test users.
    #define QSYNC_OS_SANDBOX
    static QString getSandboxDir()
    {
        QString sandbox =
                QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
        sandbox.replace(QRegularExpression("/+$"), "");
        return sandbox;
    }
    #endif
    // In a sandboxed OS, all paths must be persisted as relative paths,
    // but represented in memory as absolute paths.
    // These methods act as converters.
    static QString getSandboxRelativePath(const QString& absolutePath)
    {
    #ifdef QSYNC_OS_SANDBOX
        if (absolutePath.isEmpty())
            return absolutePath;
        return QDir(getSandboxDir()).relativeFilePath(absolutePath);
    #else
        return absolutePath;
    #endif
    }
    static QString getSandboxAbsolutePath(const QString& relativePath)
    {
    #ifdef QSYNC_OS_SANDBOX
        if (relativePath.isEmpty())
            return relativePath;
        return getSandboxDir() + "/" + relativePath;
    #else
        return relativePath;
    #endif
    }
};

#endif // QSYNC_H

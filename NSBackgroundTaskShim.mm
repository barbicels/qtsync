#import <UIKit/UIApplication.h>
#include "NSBackgroundTaskShim.h"

int taskCount = 0;

void
NSBackgroundTaskShim_beginBackgroundTask(unsigned long& taskId, std::function<bool()> handler)
{
    ++taskCount;
    [UIApplication sharedApplication].idleTimerDisabled = true;
    taskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        bool final = false;
        if (handler)
            final = handler();
        NSBackgroundTaskShim_endBackgroundTask(taskId);
        if (final)
            return;
        // If we get here, it means that the app was suspended, not killed, and so
        // we need to continue the in-progress background task.
        NSBackgroundTaskShim_beginBackgroundTask(taskId, handler);
    }];
}

void
NSBackgroundTaskShim_endBackgroundTask(unsigned long taskId)
{
    if (--taskCount <= 0)
        [UIApplication sharedApplication].idleTimerDisabled = false;
    [[UIApplication sharedApplication] endBackgroundTask:taskId];
}

double
NSBackgroundTaskShim_backgroundTimeRemaining()
{
    return [[UIApplication sharedApplication] backgroundTimeRemaining];
}

#ifndef AFILEMANAGERSHIM_H
#define AFILEMANAGERSHIM_H

#include <QtCore/QDateTime>
#include <QtCore/QFileDevice>

bool AFileManagerShim_setFileTime(const QString& filePath, const QDateTime& newDate, QFileDevice::FileTime fileTime);

#endif // AFILEMANAGERSHIM_H

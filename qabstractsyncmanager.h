#ifndef QABSTRACTSYNCMANAGER_H
#define QABSTRACTSYNCMANAGER_H

#include <QtCore/QDateTime>
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QSet>
#include <QtCore/QTimer>
#include "QThreadedUniquePtr.h"
#include "qabstractsyncmetadata.h"
#include "qabstractsyncendpoint.h"
#include "qsync.h"

class QTSYNC_EXPORT QAbstractSyncManager : public QObject
{
    Q_OBJECT
    Q_ENUM(QSyncMode)
public:
    explicit QAbstractSyncManager(QObject* parent = nullptr);
    virtual ~QAbstractSyncManager();

    // QAbstractSyncManagerState represents the manager's state at a point in time.
    // This is usually a map of endpoint states.
    class QTSYNC_EXPORT QAbstractSyncManagerState
    {
        friend QAbstractSyncManager;
    public:
        explicit QAbstractSyncManagerState() { }
        QAbstractSyncManagerState(const QAbstractSyncManagerState& other);
        virtual ~QAbstractSyncManagerState() { }
    public:
        virtual std::unique_ptr<QAbstractSyncManagerState> clone() const = 0;
        friend QDataStream& operator<<(QDataStream& stream, const QAbstractSyncManagerState& state) { state.serializeTo(stream); return stream; }
        friend QDataStream& operator>>(QDataStream& stream, QAbstractSyncManagerState& state) { state.serializeFrom(stream); return stream; }
    protected:
        enum QAbstractSyncManagerStateVersion { Q_SYNC_MANAGER_STATE_VERSION_1 = 1 };
        virtual void serializeTo(QDataStream& stream) const;
        virtual void serializeFrom(QDataStream& stream);
    private:
        QMap<QString, QSyncEndpointData> mEndpointNameToDataMap;
        QMap<QString, QByteArray> mEndpointNameToStateMap;
    };

public slots:
    void endpointEngaged(bool success);
    void endpointDisengaged(bool success);
    void endpointReported(bool success);
    void endpointWorked(bool success);
    void endpointStateChanged();
    void endpointStateMismatched();
    void syncTimerExpired();

signals:
    // signals to endpoints
    void engageEndpoint(QSyncMode mode = SyncModeInOut);
    void disengageEndpoint();
    void activateEndpoint();
    void deactivateEndpoint();
    void reportEndpoint();
    void workEndpoint();
    void abortEndpoint();
    // signals to client
    void syncStarted();
    void syncTimedOut(bool* cancel);
    void syncCanceled();
    void syncAborted(const std::exception& e);
    void syncCompleted();
    void syncReset();
    void stateChanged(const QByteArray& serializedState);
    void handleConflicts(const QStringList& items, QStringList* useEndpoints, bool* cancel);
    void endpointError(const QString& endpoint, const QString& item, const QString& detail, bool* retry, bool* abort);
    void endpointQueueLengthChanged(const QString& endpoint, int queueLength);
    void openAuthorizeDialog(const QString& title, const QUrl& url);
    void closeAuthorizeDialog();
    void openProgressDialog(const QString& message, bool* success);
    void closeProgressDialog();

public:
    QSyncMode mode() const { return mMode; }
    QAbstractSyncManagerState& state() { return *mStateUP; }
    void saveState() { mRollbackStateUP = mStateUP->clone(); Q_ASSERT(mRollbackStateUP); }
    void restoreState() { mStateUP = std::move(mRollbackStateUP); }
    QByteArray getSerializedState() const;
    void setSerializedState(QByteArray&& serializedState = QByteArray());
    QString endpointIdentity(const QString& name);
    bool insertEndpoint(const QString& name, const QSyncEndpointData& data, QMutex* accessMutexP = nullptr);
    bool removeEndpoint(const QString& name);
    bool removeEndpoints();
    bool engageEndpoints(bool wait = true, QSyncMode mode = SyncModeInOut);
    bool disengageEndpoints(bool wait = true);
    void activateEndpoints();
    void deactivateEndpoints();
    bool startSync();
    void cancelSync();
    void abortSync(const std::exception& e = QSyncException(std::string("unknown")));
    void setSyncTimeout(qint64 millisecs);
    bool dateLessThan(const QDateTime& date1, const QDateTime& date2) const;

protected:
    virtual std::unique_ptr<QAbstractSyncMetadata> metadataFactory() = 0;
    virtual bool resolveSyncConflict(const QString& item, std::map<QAbstractSyncEndpoint*, std::unique_ptr<QAbstractSyncMetadata>>& endpointPToItemMetadataUPMap) = 0;
    virtual bool filterSyncItem(const QString& item, const QAbstractSyncMetadata& metadata, const std::map<QString, std::map<QAbstractSyncEndpoint*, std::unique_ptr<QAbstractSyncMetadata>>>& itemToEndpointPToItemMetadataUPMap) = 0;
    virtual bool processSyncItem(QAbstractSyncEndpoint& fromEndpointP, const QString& item, const QAbstractSyncMetadata& metadata);

protected:
    QSyncMode mMode;
    /* persistent state */
    std::unique_ptr<QAbstractSyncManagerState> mStateUP;
    std::unique_ptr<QAbstractSyncManagerState> mRollbackStateUP;

private:
    void startSyncTimer();
    void engagementCompleted();
    void disengagementCompleted();
    void reportStarted();
    void reportCompleted();
    void workStarted();
    void workCompleted();

private:
    qint64 mSyncTimeout;
    QTimer mSyncTimer;
    std::map<QString, QThreadedUniquePtr<QAbstractSyncEndpoint>> mEndpointNameToEndpointTUPMap;
    std::map<QAbstractSyncEndpoint*, bool> mEndpointPToPollingMap;
    std::map<QAbstractSyncEndpoint*, bool> mEndpointPToEngagedMap;
    std::map<QAbstractSyncEndpoint*, bool> mEndpointPToReportedMap;
    std::map<QAbstractSyncEndpoint*, bool> mEndpointPToWorkedMap;
    std::map<QString, std::map<QAbstractSyncEndpoint*, std::unique_ptr<QAbstractSyncMetadata>>> mItemToEndpointPToItemMetadataUPMap;
    bool mSyncing;
    bool mAborting;
};

#endif // QABSTRACTSYNCMANAGER_H

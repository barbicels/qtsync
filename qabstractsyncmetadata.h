#ifndef QABSTRACTSYNCMETADATA_H
#define QABSTRACTSYNCMETADATA_H

#include "qsync.h"

struct QTSYNC_EXPORT QAbstractSyncMetadata
{
    explicit QAbstractSyncMetadata();
    virtual ~QAbstractSyncMetadata() { }

    friend QDataStream& operator<<(QDataStream& stream, const QAbstractSyncMetadata& metadata) { metadata.serializeTo(stream); return stream; }
    friend QDataStream& operator>>(QDataStream& stream, QAbstractSyncMetadata& metadata) { metadata.serializeFrom(stream); return stream; }

protected:
    virtual void serializeTo(QDataStream& stream) const = 0;
    virtual void serializeFrom(QDataStream& stream) = 0;
};

#endif // QABSTRACTSYNCMETADATA_H

#include <QtCore/QBuffer>
#include <QtCore/QCoreApplication>
#include <QtCore/QDir>
#include <QtHttpServer/QHttpServer>
#include <QtNetwork/QTcpServer>
#include <dropbox/DropboxClient.h>
#include <dropbox/endpoint/DropboxWebAuth2.h>
#include <dropbox/files/FilesDeletedMetadata.h>
#include <dropbox/files/FilesListFolderResult.h>
#include <dropbox/files/FilesRoutes.h>
#include <dropbox/users/UsersRoutes.h>
#include <stack>
#include "qdropboxsyncendpoint.h"

QDropboxSyncEndpoint::QDropboxSyncEndpoint(const QString& name, const QSyncEndpointData& data, QObject* parent)
    : QAbstractSyncEndpoint(name, DROPBOX_ENDPOINT_LOCALITY, parent),
      mAppKey(data.parameters["appKey"]),
      mAppSecret(data.parameters["appSecret"]),
      mAppName(data.parameters["appName"]),
      mRootPath(data.parameters["rootPath"])
{
    mAppInfo.setKeySecret(mAppKey, mAppSecret);
    // should check that mRootPath includes trailing "/" if not null
    mStateUP = std::make_unique<QDropboxSyncEndpointState>();
    Q_ASSERT(mStateUP);
}

void
QDropboxSyncEndpoint::QDropboxSyncMetadata::serializeTo(QDataStream& stream) const
{
    stream << mFileRevOrFolderName;
}

void
QDropboxSyncEndpoint::QDropboxSyncMetadata::serializeFrom(QDataStream& stream)
{
    stream >> mFileRevOrFolderName;
}

QDropboxSyncEndpoint::QDropboxSyncEndpointState::QDropboxSyncEndpointState() : QAbstractSyncEndpointState()
{

}

QDropboxSyncEndpoint::QDropboxSyncEndpointState::QDropboxSyncEndpointState(const QDropboxSyncEndpointState& other) : QAbstractSyncEndpointState(other)
{
    mDropboxCursor = other.mDropboxCursor;
    mItemToItemMetadataMap = other.mItemToItemMetadataMap;
}

std::unique_ptr<QAbstractSyncEndpoint::QAbstractSyncEndpointState>
QDropboxSyncEndpoint::QDropboxSyncEndpointState::clone() const
{
    return std::make_unique<QDropboxSyncEndpointState>(*this);
}

QString
QDropboxSyncEndpoint::QDropboxSyncEndpointState::getDropboxAccountId() const
{
    return mDropboxAccountId;
}

void
QDropboxSyncEndpoint::QDropboxSyncEndpointState::setDropboxAccountId(const QString& dropboxAccountId)
{
    mDropboxAccountId = dropboxAccountId;
}

QString
QDropboxSyncEndpoint::QDropboxSyncEndpointState::getDropboxAccountEmail() const
{
    return mDropboxAccountEmail;
}

void
QDropboxSyncEndpoint::QDropboxSyncEndpointState::setDropboxAccountEmail(const QString& dropboxAccountEmail)
{
    mDropboxAccountEmail = dropboxAccountEmail;
}

QString
QDropboxSyncEndpoint::QDropboxSyncEndpointState::getDropboxRefreshToken() const
{
    return mDropboxRefreshToken;
}

void
QDropboxSyncEndpoint::QDropboxSyncEndpointState::setDropboxRefreshToken(const QString& dropboxRefreshToken)
{
    mDropboxRefreshToken = dropboxRefreshToken;
}

QString
QDropboxSyncEndpoint::QDropboxSyncEndpointState::getDropboxAccessToken() const
{
    return mDropboxAccessToken;
}

void
QDropboxSyncEndpoint::QDropboxSyncEndpointState::setDropboxAccessToken(const QString& dropboxAccessToken)
{
    mDropboxAccessToken = dropboxAccessToken;
}

QString
QDropboxSyncEndpoint::QDropboxSyncEndpointState::getDropboxCursor() const
{
    return mDropboxCursor;
}

void
QDropboxSyncEndpoint::QDropboxSyncEndpointState::setDropboxCursor(const QString& dropboxCursor)
{
    mDropboxCursor = dropboxCursor;
}

QMap<QString, QDropboxSyncEndpoint::QDropboxSyncMetadata>&
QDropboxSyncEndpoint::QDropboxSyncEndpointState::getItemToItemMetadataMap()
{
    return mItemToItemMetadataMap;
}

void
QDropboxSyncEndpoint::QDropboxSyncEndpointState::serializeTo(QDataStream& stream) const
{
    QAbstractSyncEndpointState::serializeTo(stream);
    stream.setVersion(QDataStream::Qt_5_12);
    stream << (qint32) Q_DROPBOX_SYNC_ENDPOINT_STATE_VERSION_2;
    // FIX: stream dropbox user ID
    // Q_DROPBOX_SYNC_ENDPOINT_STATE_VERSION_1
    stream << mDropboxAccountId
           << mDropboxRefreshToken
           << mDropboxAccessToken
           << mDropboxCursor
           << mItemToItemMetadataMap;
    // Q_DROPBOX_SYNC_ENDPOINT_STATE_VERSION_2
    stream << mDropboxAccountEmail;
}

void
QDropboxSyncEndpoint::QDropboxSyncEndpointState::serializeFrom(QDataStream& stream)
{
    QAbstractSyncEndpointState::serializeFrom(stream);
    mDropboxAccountId = QString();
    mDropboxAccountEmail = QString();
    mDropboxRefreshToken = QString();
    mDropboxAccessToken = QString();
    mDropboxCursor = QString();
    mItemToItemMetadataMap.clear();
    stream.setVersion(QDataStream::Qt_5_12);
    qint32 version;
    stream >> version;
    switch (version)
    {
    case 0: // null
        return;
    case Q_DROPBOX_SYNC_ENDPOINT_STATE_VERSION_1:
    case Q_DROPBOX_SYNC_ENDPOINT_STATE_VERSION_2:
        break;
    default:
        throw QSyncException(std::string("unknown Dropbox sync endpoint state version: ") + std::to_string(version));
    }
    // FIX: unstream dropbox user ID and check / throw error if mismatch
    if (version >= Q_DROPBOX_SYNC_ENDPOINT_STATE_VERSION_1)
        stream >> mDropboxAccountId
               >> mDropboxRefreshToken
               >> mDropboxAccessToken
               >> mDropboxCursor
               >> mItemToItemMetadataMap;
    if (version >= Q_DROPBOX_SYNC_ENDPOINT_STATE_VERSION_2)
        stream >> mDropboxAccountEmail;
}

QString
QDropboxSyncEndpoint::identity() const
{
    auto& endpointState = *dynamic_cast<QDropboxSyncEndpointState*>(mStateUP.get());
    auto email = endpointState.getDropboxAccountEmail();
    if (email.isEmpty())
        return "Dropbox";
    return QString("Dropbox: %1").arg(email);
}

bool
QDropboxSyncEndpoint::dateLessThan(const QDateTime& date1, const QDateTime& date2) const
{
    // dropboxQt renders dates in this format, so differences of less than a second are not significant.
    const QString dateFormat("yyyy-MM-ddThh:mm:ssZ");
    return date1.toString(dateFormat) < date2.toString(dateFormat);
}

bool
QDropboxSyncEndpoint::engage(QSyncMode mode)
{   qDebug() << "dropbox: engage\n";
    Q_UNUSED(mode)
    auto& endpointState = *dynamic_cast<QDropboxSyncEndpointState*>(mStateUP.get());
    if (!endpointState.getDropboxAccessToken().isEmpty())
    {
        mClientUP = std::make_unique<DropboxClient>(endpointState.getDropboxAccessToken());
        Q_ASSERT(mClientUP);
        if (isOffline())
            return false;
        if (!isReady())
        {   // Possibly, the token is expired.
            endpointState.setDropboxAccessToken();
        }
    }
    if (endpointState.getDropboxAccessToken().isEmpty())
    {
        mClientUP.reset();
        if (!endpointState.getDropboxRefreshToken().isEmpty())
        {
            try
            {
                auto authInfo = DropboxWebAuth2::getTokenFromRefreshToken(mAppInfo, endpointState.getDropboxRefreshToken());
                endpointState.setDropboxAccessToken(authInfo.getAccessToken());
            }
            catch (const DropboxException&)
            {   // Possibly, the user has revoked permission or no longer exists.
                endpointState.setDropboxRefreshToken();
            }
        }
        if (endpointState.getDropboxRefreshToken().isEmpty())
        {
            QHttpServer httpServer;
            class LoopbackListener
            {
            public:
                LoopbackListener(const QString& antiCSRFState, QString& redirectURL, QString& accessCode)
                    : mAntiCSRFState(antiCSRFState), mAccessCode(accessCode)
                {
                    // These port numbers are registered in the Dropbox app's console as part of valid redirect URLs.
                    constexpr unsigned int registeredPorts[] = { 58280, 55104, 53006, 64613, 64010, 49162, 60625, 56801, 64975, 60955 };
                    for (auto const port : registeredPorts)
                        if (mHttpServer.listen(QHostAddress::LocalHost, port) != 0)
                            break;
                    auto const& servers = mHttpServer.servers();
                    auto const& serverPorts = mHttpServer.serverPorts();
                    if (servers.size() < 1 || serverPorts.size() < 1 || serverPorts[0] == 0)
                        throw QSyncException(std::string("failed to open listening port for access code from Dropbox"));
                    redirectURL = QString("http://%1:%2/%3").arg(servers[0]->serverAddress().toString()).arg(serverPorts[0]).arg(mRedirectRoute);
                    if (!mHttpServer.route("/" + mQuitRoute, [] (const QHttpServerRequest& request) -> QString
                    {
                        Q_UNUSED(request)
                        QThread::currentThread()->quit();
                        return QString();
                    }) || !mHttpServer.route("/" + mRedirectRoute, [this] (const QHttpServerRequest& request) -> QString
                    {
                        QString resultText;
                        auto const query = request.query();
                        auto const error = query.queryItemValue("error");
                        if (error.isEmpty())
                        {
                            auto const state = query.queryItemValue("state", QUrl::FullyDecoded).replace('+', ' ');
                            if (state == mAntiCSRFState)
                            {
                                auto const code = query.queryItemValue("code");
                                if (code.isEmpty())
                                    resultText = "Dropbox failed to return an access code.";
                                else
                                {
                                    mAccessCode = code;
                                    resultText = "Access to Dropbox has been granted.";
                                }
                            }
                            else
                                resultText = "Dropbox returned an invalid anti-CSRF state.";
                        }
                        else
                            resultText = "Dropbox returned an error: <code>" + error + "</code>";
                        // Linking to /quit forces the listener thread to quit, but only after the browser gets this reply.
                        return QString("<head>"
                                       "<title>Dropbox Authorization %1</title>"
                                       "<link rel='stylesheet' href='/quit'>"
                                       "<style>body { font-family: -apple-system, Roboto, sans-serif; }</style>"
                                       "</head>"
                                       "<h1>Dropbox Authorization %1</h1>"
                                       "<p>%2</p>"
                                       "<p><b>Please close this page to return to the requesting application.</b></p>")
                                .arg(mAccessCode.isEmpty() ? "Failed" : "Succeeded", resultText);
                    }))
                        throw QSyncException(std::string("failed to listen for access code from Dropbox"));
                }
            private:
                const QString mRedirectRoute = "redirect";
                const QString mQuitRoute = "quit";
                const QString& mAntiCSRFState;
                QHttpServer mHttpServer;
                QString& mAccessCode;
            };

            QString redirectURL;
            QString accessCode;
            {   // Engage with loopback listener in a separate thread.
                auto const antiCSRFState = QDateTime::currentDateTime().toString();
                auto loopbackListenerTUP = std::make_unique_threaded<LoopbackListener>(this, antiCSRFState, redirectURL, accessCode);
                Q_ASSERT(loopbackListenerTUP);
                connect(loopbackListenerTUP.getThreadUP().get(), &QThread::finished, [this] { emit closeAuthorizeDialog(); });
                emit openAuthorizeDialog("Connect to Dropbox",
                                         DropboxWebAuth2::getCodeAuthorizeUrl(mAppInfo, redirectURL, antiCSRFState, true));
                if (loopbackListenerTUP.getThreadUP()->isRunning())
                    loopbackListenerTUP.getThreadUP()->quit();
                if (accessCode.isEmpty())
                    throw QSyncException(std::string("failed to receive Dropbox access code"));
                // Loopback listener is destroyed here.
            }

            auto const existingAccountId = endpointState.getDropboxAccountId();
            DropboxAuthInfo2 authInfo;
            try
            {   // This is where we'd first encounter an error due to QtNetwork, such as missing OpenSSL.
                authInfo = DropboxWebAuth2::getTokenFromCode(mAppInfo, accessCode, redirectURL);
            }
            catch (const DropboxException& e)
            {
                if (e.statusCode() != 0)
                    throw;
                throw QSyncException(std::string("failed to get Dropbox access token (probably due to missing SSL support or other networking failure)"));
            }
            if (!existingAccountId.isEmpty() && authInfo.getAccountId() != existingAccountId)
            {   // If now using a different account, then all state is mismatched and full resync is required.
                emit endpointStateMismatched();
                return false;
            }
            endpointState.setDropboxAccountId(authInfo.getAccountId());
            endpointState.setDropboxRefreshToken(authInfo.getRefreshToken());
            endpointState.setDropboxAccessToken(authInfo.getAccessToken());
        }
        mClientUP = std::make_unique<DropboxClient>(endpointState.getDropboxAccessToken());
        Q_ASSERT(mClientUP);
        bool abort = false;
        auto const fullAccount = guardedCall(abort, nullptr,
                                             &DropboxClient::getUsers, &users::UsersRoutes::getCurrentAccount);
        if (!fullAccount)
            throw QSyncException(std::string("failed to get current account after linking"));
        endpointState.setDropboxAccountEmail(fullAccount ? fullAccount->email() : QString());
    }
    return true;
}

bool
QDropboxSyncEndpoint::disengage()
{   qDebug() << "dropbox: disengage\n";
    // FIX: Cancel long-running asynchronous tasks here.
    mClientUP.reset();
    return true;
}

bool
QDropboxSyncEndpoint::report(const bool& abort)
{   qDebug() << "dropbox: report\n";
    mReportedItems = QStringList();
    mItemToFileMetadataUPMap.clear();
    mItemToFolderMetadataUPMap.clear();
    mItemToDeletedMetadataUPMap.clear();
    auto& endpointState = *dynamic_cast<QDropboxSyncEndpointState*>(mStateUP.get());
    auto& itemMetadataMap = endpointState.getItemToItemMetadataMap();
    auto const processEntries = [this, &itemMetadataMap, &abort] (const files::ListFolderResult& result)
    {
        for (auto const& entry : result.entries())
        {
            auto const& metadata = *entry;
            auto const item = metadata.pathDisplay().remove(0, mRootPath.length() + 1).toLower(); // remove leading "/"
            auto const inMetadataMap = itemMetadataMap.contains(item);
            if (!item.isEmpty())
            {
                auto const* const fileMetadataP = dynamic_cast<const files::FileMetadata*>(&metadata);
                if (fileMetadataP != nullptr)
                {
                    auto const& fileMetadata = *fileMetadataP;
                    if (!inMetadataMap || fileMetadata.rev() != itemMetadataMap[item].mFileRevOrFolderName)
                    {
                        mReportedItems << item;
                        auto& fileMetadataUP = mItemToFileMetadataUPMap[item];
                        fileMetadataUP = std::make_unique<const files::FileMetadata>(fileMetadata);
                        Q_ASSERT(fileMetadataUP);
                        itemMetadataMap[item].mFileRevOrFolderName = fileMetadata.rev();
                    }
                }
                else
                {
                    auto const* const folderMetadataP = dynamic_cast<const files::FolderMetadata*>(&metadata);
                    if (folderMetadataP != nullptr)
                    {
                        auto const& folderMetadata = *folderMetadataP;
                        if (!inMetadataMap || folderMetadata.name() != itemMetadataMap[item].mFileRevOrFolderName)
                        {
                            mReportedItems << item;
                            auto& folderMetadataUP = mItemToFolderMetadataUPMap[item];
                            folderMetadataUP = std::make_unique<const files::FolderMetadata>(folderMetadata);
                            Q_ASSERT(folderMetadataUP);
                            itemMetadataMap[item].mFileRevOrFolderName = folderMetadata.name();
                        }
                    }
                    else
                    {
                        auto const* const deletedMetadataP = dynamic_cast<const files::DeletedMetadata*>(&metadata);
                        if (deletedMetadataP != nullptr)
                        {
                            auto const& deletedMetadata = *deletedMetadataP;
                            if (inMetadataMap)
                            {
                                mReportedItems << item;
                                auto& deletedMetadataUP = mItemToDeletedMetadataUPMap[item];
                                deletedMetadataUP = std::make_unique<const files::DeletedMetadata>(deletedMetadata);
                                Q_ASSERT(deletedMetadataUP);
                                itemMetadataMap.remove(item);
                            }
                        }
                    }
                }
            }
            if (abort)
                return;
        }
    };
    bool hasMore;
    auto dropboxCursor = endpointState.getDropboxCursor();
    if (dropboxCursor.isEmpty())
    {
        auto const listFolderArg = files::ListFolderArg(mRootPath)
                .setRecursive(true)
                .setIncludedeleted(true);
        auto const resultUP = guardedCall(abort, nullptr,
                                          &DropboxClient::getFiles, &files::FilesRoutes::listFolder, listFolderArg);
        if (!resultUP)
            throw QSyncException(std::string("failed to fetch list of changed items from Dropbox"));
        auto const& result = *resultUP;
        processEntries(result);
        hasMore = result.hasMore();
        dropboxCursor = result.cursor();
    }
    else
        hasMore = true;
    while (hasMore && !abort)
    {
        auto const listFolderContinueArg = files::ListFolderContinueArg(dropboxCursor);
        std::unique_ptr<files::ListFolderResult> resultUP;
        try
        {
            resultUP = guardedCall(abort, nullptr,
                                   &DropboxClient::getFiles, &files::FilesRoutes::listFolderContinue, listFolderContinueArg);
            if (!resultUP)
                throw QSyncException(std::string("failed to fetch list of changed items from Dropbox"));
        }
        catch (const files::ListFolderContinueErrorException& e)
        {
            // Don't throw an error if the cursor was invalidated by the deletion of an online folder.
            if (e.err().tag() != files::ListFolderContinueError::ListFolderContinueError_RESET)
                throw;
            // When a cursor is invalidated, all state is mismatched and full resync is required.
            emit endpointStateMismatched();
            return false;
        }
        auto const& result = *resultUP;
        processEntries(result);
        hasMore = result.hasMore();
        dropboxCursor = result.cursor();
    }
    if (abort)
        return false;
    endpointState.setDropboxCursor(dropboxCursor);
    return true;
}

void
QDropboxSyncEndpoint::stopActivity()
{
    if (mClientUP)
        mClientUP->cancellAll();
}

void
QDropboxSyncEndpoint::getFileMetadata(const QString& item, QFileSyncManager::QFileSyncMetadata& fileMetadata) const
{
    if (mItemToFileMetadataUPMap.find(item) != mItemToFileMetadataUPMap.cend())
    {
        auto const& dropboxFileMetadata = *mItemToFileMetadataUPMap.at(item);
        auto lastModified = dropboxFileMetadata.clientModified();
        auto dropboxHash = dropboxFileMetadata.contentHash();
        lastModified.setTimeSpec(Qt::UTC);
        fileMetadata.mLastModified = lastModified.toLocalTime();
        fileMetadata.mVersion = dropboxFileMetadata.rev();
        fileMetadata.mDropboxHashGetter = [dropboxHash] { return dropboxHash; };
        fileMetadata.mSize = dropboxFileMetadata.size();
        fileMetadata.mCaseSensitivePath = dropboxFileMetadata.pathDisplay().right(item.length());
    } else if (mItemToFolderMetadataUPMap.find(item) != mItemToFolderMetadataUPMap.cend())
    {
        auto const& dropboxFolderMetadata = *mItemToFolderMetadataUPMap.at(item);
        fileMetadata.mIsFolder = true;
        // Note:  Dropbox does not record the last-modified date of folders.
        fileMetadata.mCaseSensitivePath = dropboxFolderMetadata.pathDisplay().right(item.length());
    } else if (mItemToDeletedMetadataUPMap.find(item) != mItemToDeletedMetadataUPMap.cend())
    {
        auto const& dropboxDeletedMetadata = *mItemToDeletedMetadataUPMap.at(item);
        fileMetadata.mIsDeleted = true;
        fileMetadata.mCaseSensitivePath = dropboxDeletedMetadata.pathDisplay().right(item.length());
    }
}

void
QDropboxSyncEndpoint::getItemMetadata(const QString& item, QAbstractSyncMetadata& metadata)
{
    auto* const fileMetadataP = dynamic_cast<QFileSyncManager::QFileSyncMetadata*>(&metadata);
    if (fileMetadataP != nullptr)
    {
        auto& fileMetadata = *fileMetadataP;
        getFileMetadata(item, fileMetadata);
    }
    else
        throw QSyncException(std::string("metadata class unknown to Dropbox sync endpoint: ") + item.toStdString());
}

std::shared_ptr<QAbstractSyncEndpoint::QAbstractSyncEndpointWorkItem>
QDropboxSyncEndpoint::workItemFactory(const QString& item, const QAbstractSyncMetadata* metadataP)
{
    auto bufferArrayUP = std::make_unique<QByteArray>();
    Q_ASSERT(bufferArrayUP);
    auto workItemSP = std::make_shared<QDropboxSyncEndpointWorkItem>();
    Q_ASSERT(workItemSP);
    workItemSP->ready = false;
    workItemSP->canceled = false;
    workItemSP->fromEndpointP = this;
    workItemSP->item = item;
    workItemSP->metadataP = metadataP;
    workItemSP->deviceUP = std::make_unique<QBuffer>(bufferArrayUP.get());
    Q_ASSERT(workItemSP->deviceUP);
    workItemSP->bufferArrayUP = std::move(bufferArrayUP);
    return workItemSP;
}

bool
QDropboxSyncEndpoint::provideItem(QAbstractSyncEndpointWorkItem& workItem, const bool& abort)
{   qDebug() << "dropbox: provideItem (" + workItem.item + ")\n";
    auto const* const fileMetadataP = dynamic_cast<const QFileSyncManager::QFileSyncMetadata*>(workItem.metadataP);
    if (fileMetadataP != nullptr)
    {
        auto const& fileMetadata = *fileMetadataP;
        auto const dropboxPath = mRootPath + "/" + fileMetadata.mCaseSensitivePath;
        if (fileMetadata.mIsFolder || fileMetadata.mIsDeleted)
            ;   // folder or deleted: nothing to do
        else
        {   // file
            if (!(workItem.deviceUP && workItem.deviceUP->isWritable()))
                return false;
            auto const downloadArg = files::DownloadArg(dropboxPath);
            try
            {
                if (!guardedCall(abort, [&workItem] { workItem.deviceUP->seek(0); }, &DropboxClient::getFiles,
                                 &files::FilesRoutes::download, downloadArg, workItem.deviceUP.get()))
                    throw QSyncException(std::string("failed to read file: ") + workItem.item.toStdString());
            }
            catch (const files::DownloadErrorException& e)
            {   // BUG: Dropbox returns error 409 with no usable tags if file is deleted before or during transfer.
                if (e.statusCode() != 409)
                    throw;
                return false;
            }
        }
        return true;
    }
    else
        throw QSyncException(std::string("metadata class unknown to Dropbox sync endpoint: ") + workItem.item.toStdString());
}

bool
QDropboxSyncEndpoint::consumeItem(QAbstractSyncEndpointWorkItem& workItem, const bool& abort)
{   qDebug() << "dropbox: consumeItem (" + workItem.item + ")\n";
    auto& endpointState = *dynamic_cast<QDropboxSyncEndpointState*>(mStateUP.get());
    auto& itemMetadataMap = endpointState.getItemToItemMetadataMap();
    auto const* const fileMetadataP = dynamic_cast<const QFileSyncManager::QFileSyncMetadata*>(workItem.metadataP);
    if (fileMetadataP != nullptr)
    {
        auto const& fileMetadata = *fileMetadataP;
//        // Note:  There may be no mCaseSensitivePath in the case of deleted items.
//        auto const dropboxPath = mRootPath + "/"
//                + (fileMetadata.mCaseSensitivePath.isEmpty() ? workItem.item : fileMetadata.mCaseSensitivePath);
        auto const dropboxPath = mRootPath + "/" + fileMetadata.mCaseSensitivePath;
        std::unique_ptr<files::Metadata> metadataUP;
        auto const rectifyDropboxPath = [this, &abort, &dropboxPath, &metadataUP] ()
        {
            if (metadataUP->name() != dropboxPath.right(metadataUP->name().length()))
            {   // Case has changed.  Dropbox API doesn't allow a direct case change.
                auto dummyPath = dropboxPath;
                forever
                {
                    dummyPath += "-";
                    try
                    {
                        auto relocationArg = files::RelocationArg(dropboxPath)
                                .setTopath(dummyPath);
                        if (!guardedCall(abort, nullptr,
                                         &DropboxClient::getFiles, &files::FilesRoutes::move, relocationArg))
                            throw QSyncException(std::string("failed to rename item: ") + dropboxPath.toStdString());
                        break;
                    }
                    catch (const files::RelocationErrorException& e)
                    {
                        if (e.err().tag() != files::RelocationError::RelocationError_TO
                                || e.err().getTo().tag() != files::WriteError::WriteError_CONFLICT)
                            throw;
                    }
                }
                auto relocationArg = files::RelocationArg(dummyPath)
                        .setTopath(dropboxPath);
                metadataUP = guardedCall(abort, nullptr,
                                         &DropboxClient::getFiles, &files::FilesRoutes::move, relocationArg);
                if (!metadataUP)
                    throw QSyncException(std::string("failed to rename item: ") + dropboxPath.toStdString());
            }
        };
        // Ignore Dropbox-illegal characters, but only when deleting items that shouldn't exist on Dropbox anyway.
        if (workItem.item.indexOf(QRegularExpression("[\\\\”<>:\\|\\?\\*]")) >= 0)
        {
            if (fileMetadata.mIsDeleted)
                return true;
            throw QSyncException(std::string("cannot upload item due to illegal name: ") + dropboxPath.toStdString());
        }
        if (fileMetadata.mIsFolder)
        {   // folder
            // Note:  Dropbox does not record the last-modified date of folders.
            try
            {
                auto const getMetadataArg = files::GetMetadataArg(dropboxPath);
                metadataUP = guardedCall(abort, nullptr,
                                         &DropboxClient::getFiles, &files::FilesRoutes::getMetadata, getMetadataArg);
                if (!metadataUP)
                    throw QSyncException(std::string("failed to get folder metadata: ") + dropboxPath.toStdString());
                if (dynamic_cast<files::FolderMetadata*>(metadataUP.get()) != nullptr)
                    rectifyDropboxPath();   // may update metadataUP
                itemMetadataMap[workItem.item].mFileRevOrFolderName = dropboxPath.right(metadataUP->name().length());
            }
            catch (const files::GetMetadataErrorException& e)
            {   // Don't throw an error if there is no conflict.
                if (e.err().tag() != files::GetMetadataError::GetMetadataError_PATH
                        || e.err().getPath().tag() != files::LookupError::LookupError_NOT_FOUND)
                    throw;
                auto const createFolderArg = files::CreateFolderArg(dropboxPath);
                metadataUP = guardedCall(abort, nullptr,
                                         &DropboxClient::getFiles, &files::FilesRoutes::createFolder, createFolderArg);
                if (!metadataUP)
                    throw QSyncException(std::string("failed to get folder metadata: ") + dropboxPath.toStdString());
                itemMetadataMap[workItem.item].mFileRevOrFolderName = metadataUP->name();
            }
        }
        else if (fileMetadata.mIsDeleted)
        {   // deleted
            try
            {
                auto const deleteArg = files::DeleteArg(dropboxPath);
                if (!guardedCall(abort, nullptr,
                                 &DropboxClient::getFiles, &files::FilesRoutes::deleteOperation, deleteArg))
                    throw QSyncException(std::string("failed to delete item: ") + dropboxPath.toStdString());
            }
            catch (const files::DeleteErrorException& e)
            {
                // Don't throw an error if this is just a conflict with a non-existent item.
                if (e.err().tag() != files::DeleteError::DeleteError_PATH_LOOKUP
                        || e.err().getPathLookup().tag() != files::LookupError::LookupError_NOT_FOUND)
                    throw;
            }
            itemMetadataMap.remove(workItem.item);
        }
        else
        {   // file
            if (!(workItem.deviceUP && workItem.deviceUP->isReadable()))
                return false;
            // Device data is available, so upload file.
            auto const oldRev = itemMetadataMap.contains(workItem.item) ? itemMetadataMap[workItem.item].mFileRevOrFolderName : QString();
            auto const mode = oldRev.isEmpty()
                    ? files::WriteMode(files::WriteMode::WriteMode_OVERWRITE)
                    : files::WriteMode(files::WriteMode::WriteMode_UPDATE).setUpdate(oldRev);
            auto const commitInfo = files::CommitInfo(dropboxPath)
                    .setMute(true) // note, folder creations can't be muted
                    .setMode(mode)
                    .setClientmodified(fileMetadata.mLastModified.toUTC());
            // Dropbox API v2 allows uploads of up to 150 MB without opening an upload session.
            // For larger files, API documentation says that appended chunks should be a multiple
            // of 4 MB in size (except for the last chunk), so we use 148 MB as our chunk size.
            qint64 constexpr MAX_UPLOAD_SIZE = 37 * (4 * 1024 * 1024);
            if (workItem.deviceUP->bytesAvailable() > MAX_UPLOAD_SIZE)
            {
                int offset = 0;
                auto const uploadSessionStartArg = files::UploadSessionStartArg();
                auto const uploadSessionResult = guardedCall(abort, nullptr,
                                                             &DropboxClient::getFiles, &files::FilesRoutes::uploadSessionStart, uploadSessionStartArg, nullptr);
                if (!uploadSessionResult)
                    throw QSyncException(std::string("failed to upload file: ") + dropboxPath.toStdString());
                auto uploadSessionCursor = files::UploadSessionCursor(uploadSessionResult->sessionId());
                auto uploadSessionAppendArg = files::UploadSessionAppendArg();
                auto uploadSessionFinishArg = files::UploadSessionFinishArg()
                        .setCommit(commitInfo);
                QBuffer partBuffer;
                if (!partBuffer.open(QBuffer::ReadWrite))
                    throw QSyncException(std::string("failed to upload file due to buffering failure: ") + workItem.item.toStdString());
                while (workItem.deviceUP->bytesAvailable() > MAX_UPLOAD_SIZE)
                {
                    partBuffer.seek(0);
                    auto const bytesProcessed = partBuffer.write(workItem.deviceUP->read(MAX_UPLOAD_SIZE));
                    if (!guardedCall(abort, [&] { uploadSessionAppendArg.setCursor(uploadSessionCursor.setOffset(offset)); partBuffer.seek(0); },
                                     &DropboxClient::getFiles, &files::FilesRoutes::uploadSessionAppendV2, uploadSessionAppendArg, &partBuffer))
                        throw QSyncException(std::string("failed to upload file: ") + dropboxPath.toStdString());
                    offset += bytesProcessed;
                }
                try
                {
                    metadataUP = guardedCall(abort, [&] { uploadSessionFinishArg.setCursor(uploadSessionCursor.setOffset(offset)); workItem.deviceUP->seek(offset); },
                                             &DropboxClient::getFiles, &files::FilesRoutes::uploadSessionFinish, uploadSessionFinishArg, workItem.deviceUP.get());
                    if (!metadataUP)
                        throw QSyncException(std::string("failed to upload file: ") + dropboxPath.toStdString());
                }
                catch (const files::UploadSessionFinishErrorException& e)
                {
                    // Don't throw a Dropbox exception if this is just an insufficient-space condition.
                    // Also, ignore incorrect-offset errors caused by repetition in guardedCall.
                    if ((e.err().tag() != files::UploadSessionFinishError::UploadSessionFinishError_PATH
                         || e.err().getPath().tag() != files::WriteError::WriteError_INSUFFICIENT_SPACE) &&
                            (e.err().tag() != files::UploadSessionFinishError::UploadSessionFinishError_LOOKUP_FAILED
                             || e.err().getLookupFailed().tag() != files::UploadSessionLookupError::UploadSessionLookupError_INCORRECT_OFFSET
                             || e.err().getLookupFailed().getIncorrectOffset().correctOffset() != offset))
                        throw;
                    throw QSyncException(std::string("failed to upload file due to insufficient space: ") + dropboxPath.toStdString());
                }
            }
            else
            {
                try
                {
                    metadataUP = guardedCall(abort, [&workItem] { workItem.deviceUP->seek(0); },
                                             &DropboxClient::getFiles, &files::FilesRoutes::upload, commitInfo, workItem.deviceUP.get());
                    if (!metadataUP)
                        throw QSyncException(std::string("failed to upload file: ") + dropboxPath.toStdString());
                }
                catch (const files::UploadErrorException& e)
                {
                    // Don't throw an error if this is just an insufficient-space condition.
                    if (e.err().tag() != files::UploadError::UploadError_PATH
                            || e.err().getPath().reason().tag() != files::WriteError::WriteError_INSUFFICIENT_SPACE)
                        throw;
                    throw QSyncException(std::string("failed to upload file due to insufficient space: ") + dropboxPath.toStdString());
                }
            }
            rectifyDropboxPath();   // may update metadataUP
            itemMetadataMap[workItem.item].mFileRevOrFolderName = dynamic_cast<files::FileMetadata*>(metadataUP.get())->rev();
        }
        return true;
    }
    else
        throw QSyncException(std::string("metadata class unknown to Dropbox sync endpoint: ") + workItem.item.toStdString());
}

QStringList
QDropboxSyncEndpoint::getCanonicalPaths()
{   qDebug() << "dropbox: getCanonicalPaths\n";
    bool abort = false;
    auto const fullAccount = guardedCall(abort, nullptr,
                                         &DropboxClient::getUsers, &users::UsersRoutes::getCurrentAccount);
    if (!fullAccount)
        return QStringList();

    QStringList canonicalPaths;

    canonicalPaths << QString("dropbox://%1%2/%3")
                          .arg(fullAccount->accountId())
                          .arg(mAppName.isEmpty() ? "" : "/Apps/" + mAppName)
                          .arg(mRootPath);

#if defined(Q_OS_IOS) || defined(Q_OS_ANDROID)
    // Mobile platforms sandbox their apps' data, so it's inaccessible.
#else
#ifdef Q_OS_WIN
    QFile infoFile(QStandardPaths::locate(QStandardPaths::GenericConfigLocation, "Dropbox/info.json"));
#else
    QFile infoFile(QDir::home().filePath(".dropbox/info.json"));
#endif
    if (infoFile.open(QFile::ReadOnly))
    {
        auto const processJson = [this, &canonicalPaths] (const QJsonObject& json)
        {
            auto const& keys = json.keys();
            auto const& key = keys[0];
            if (key == "personal" || key == "business")
            {
                auto const& value = json[key];
                if (value.isObject())
                {
                    auto const& dropboxPath = value.toObject()["path"].toString();
                    if (!dropboxPath.isEmpty())
                    {
                        QDir dropboxDir(dropboxPath);
                        if (dropboxDir.exists())
                            canonicalPaths << QString("folder://%1%2/%3")
                                                  .arg(dropboxDir.canonicalPath().replace(QDir::separator(), '/'))
                                                  .arg(mAppName.isEmpty() ? "" : "/Apps/" + mAppName)
                                                  .arg(mRootPath);
                    }
                }
            }
        };
        auto jsonDocument = QJsonDocument::fromJson(infoFile.readAll());
        if (jsonDocument.isArray())
        {
            auto const& array = jsonDocument.array();
            for (auto const& json : array)
                if (json.isObject())
                    processJson(json.toObject());
        }
        else if (jsonDocument.isObject())
            processJson(jsonDocument.object());
    }
#endif

    return canonicalPaths;
}

bool
QDropboxSyncEndpoint::isOffline()
{
    if (!mClientUP)
        return true;

    // FIX: The check/app endpoint might be more appropriate, once it's out of test.
    try
    {
        (void) mClientUP->getUsers()->getCurrentAccount();
        return false;
    }
    catch (const DropboxException& e)
    {
        if (e.statusCode() == 0)
            return true;
        return false;
    }
}

bool
QDropboxSyncEndpoint::isReady()
{
    if (!mClientUP)
        return false;

    // FIX: The check/app endpoint might be more appropriate, once it's out of test.
    try
    {
        auto const fullAccount = mClientUP->getUsers()->getCurrentAccount();
        if (!fullAccount)
            return false;
        return !fullAccount->disabled();
    }
    catch (const DropboxException&)
    {
        // This includes any condition that prevents network access.
        return false;
    }
}

void
QDropboxSyncEndpoint::getClient(const bool& abort)
{
    if (!isReady())
    {
        if (abort)
            return;
        disengageEndpoint();
        if (abort)
            return;
        engageEndpoint(mMode);
        if (abort)
            return;
        if (!isReady())
            throw QSyncException(std::string("Dropbox sync endpoint not engaged"));
    }
}

template<typename Routes, typename Result, typename... Args, typename... ArgsAsSupplied>
Result
QDropboxSyncEndpoint::guardedCall(const bool& abort, std::function<void()> setup, Routes* (DropboxClient::*gP)(), Result (Routes::*fP)(Args...), ArgsAsSupplied&&... a)
{
    bool retry = !mClientUP;
    forever
    {
        if (abort)
            return Result();
        try
        {
            if (retry)
            {
                getClient(abort);
                if (abort || !mClientUP)
                    return Result();
            }
            if (setup)
                setup();
            auto result = (((*mClientUP).*gP)()->*fP)(std::forward<ArgsAsSupplied>(a)...);
            if (abort)
                return Result();
            return result;
        }
        catch (const DropboxException&)
        {
            if (retry)
                throw;
            retry = true;
        }
    }
}

#import <Foundation/Foundation.h>
#include "NSFileManagerShim.h"

bool
NSFileManagerShim_setFileTime(const QString& filePath, const QDateTime& newDate, QFileDevice::FileTime fileTime)
{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* convertedFilePath = filePath.toNSString();
    NSFileAttributeKey fileAttributeKey =
            fileTime == QFileDevice::FileModificationTime ? NSFileModificationDate :
            fileTime == QFileDevice::FileBirthTime ? NSFileCreationDate :
            nullptr;
    if (fileAttributeKey == nullptr)
        return false;
    NSDate* fileAttributeDate =
//            newDate.toNSDate();
            [NSDate dateWithTimeIntervalSince1970:static_cast<NSTimeInterval>(newDate.toMSecsSinceEpoch()) / 1000];
    NSDictionary<NSFileAttributeKey, id>* fileAttributes = @{fileAttributeKey:fileAttributeDate};
    NSError* error;
    if (![fileManager setAttributes:fileAttributes ofItemAtPath:convertedFilePath error:&error])
    {
        NSLog(@"setFileTime error: %@", [error localizedDescription]);
        return false;
    }
    return true;
}

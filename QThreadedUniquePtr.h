#ifndef QTHREADEDUNIQUEPTR_H
#define QTHREADEDUNIQUEPTR_H

#include <cxxabi.h>
#include <QtCore/QThread>
#include <QtCore/QSettings>
#include <QtCore/QTimer>


// FIX: consider making this a derived class of std::unique_ptr

template<class T, class... TArgs>
class QThreadedUniquePtr
{
public:
    explicit QThreadedUniquePtr(QObject* parent = nullptr, TArgs&&... args);
    template<class U, class... UArgs>
    QThreadedUniquePtr(const QThreadedUniquePtr<U, UArgs...>& other) = delete;
    template<class U, class... UArgs>
    QThreadedUniquePtr& operator=(const QThreadedUniquePtr<U, UArgs...>& other) = delete;
    template<class U, class... UArgs, typename = std::enable_if<std::is_convertible_v<U*, T*>>>
    QThreadedUniquePtr(QThreadedUniquePtr<U, UArgs...>&& other);
    template<class U, class... UArgs, typename = std::enable_if<std::is_convertible_v<U*, T*>>>
    QThreadedUniquePtr& operator=(QThreadedUniquePtr<U, UArgs...>&& other);

    class Thread : public QThread
    {
    public:
        explicit Thread(QObject* parent = nullptr, TArgs&&... args);
        ~Thread();
    public:
        T* get() const { return mSubjectUP.get(); }
    protected:
        virtual void run() override;
    private:
        std::unique_ptr<T> mSubjectUP;
        std::function<std::unique_ptr<T>()> mAllocator;
//        friend class QThreadedUniquePtr;
    };
    friend class Thread;

public:
    T* operator->() const { return get(); }
    T& operator*() const { return *get(); }
    T* get() const { if (subjectP) return subjectP; auto* threadP = dynamic_cast<Thread*>(mThreadUP.get()); if (!threadP) return nullptr; return threadP->get(); }
    operator bool() const { return (bool) get(); }
    // This should probably just be getThreadP() const.
    std::unique_ptr<QThread>& getThreadUP() { return mThreadUP; }
    T* getSubjectP() const { return subjectP; }

private:
    std::unique_ptr<QThread> mThreadUP;
    T* subjectP;
};

namespace std
{
    template<class T, class... TArgs>
    inline
    QThreadedUniquePtr<T, TArgs...>
    make_unique_threaded(QObject* parent, TArgs&&... __args)
    {
        return QThreadedUniquePtr<T, TArgs...>(parent, std::forward<TArgs>(__args)...);
    }
}

template<class T, class... TArgs>
QThreadedUniquePtr<T, TArgs...>::QThreadedUniquePtr(QObject *parent, TArgs&&... args)
    : mThreadUP(std::make_unique<Thread>(parent, std::forward<TArgs>(args)...)), subjectP(nullptr)
{
    Q_ASSERT(mThreadUP.get());
    int status;
    std::unique_ptr<char, decltype(free)*> demangledNameUP(abi::__cxa_demangle(typeid(T).name(), 0, 0, &status), free);
    if(status == 0 && demangledNameUP)
        mThreadUP->setObjectName(QString(demangledNameUP.get()).section(':', -1));
    if constexpr (!std::is_abstract_v<T>)
    {
        mThreadUP->start();
        while (!(subjectP = get()))
            QTimer::singleShot(20, [] { });
    }
}

template<class T, class... TArgs>
template<class U, class... UArgs, typename>
QThreadedUniquePtr<T, TArgs...>::QThreadedUniquePtr(QThreadedUniquePtr<U, UArgs...>&& other)
{
    mThreadUP = std::move(other.getThreadUP());
    subjectP = other.getSubjectP();
}

template<class T, class... TArgs>
template<class U, class... UArgs, typename>
QThreadedUniquePtr<T, TArgs...>&
QThreadedUniquePtr<T, TArgs...>::operator=(QThreadedUniquePtr<U, UArgs...>&& other)
{
    mThreadUP = std::move(other.getThreadUP());
    subjectP = other.getSubjectP();
    return *this;
}

template<class T, class... TArgs>
QThreadedUniquePtr<T, TArgs...>::Thread::Thread(QObject* parent, TArgs&&... args)
    : QThread(parent), mSubjectUP(std::unique_ptr<T>())
{
    if constexpr (std::is_abstract_v<T>)
        mAllocator = [] () -> std::unique_ptr<T> { return std::unique_ptr<T>(); };
    else
    {
        mAllocator = [&args...] () -> std::unique_ptr<T> { return std::make_unique<T>(std::forward<TArgs>(args)...); };
        start();
    }
}

template<class T, class... TArgs>
QThreadedUniquePtr<T, TArgs...>::Thread::~Thread()
{
    if (isRunning())
    {
        quit();
        // wait() has been observed to block endlessly on iOS, triggering the watchdog.
        // So let's just watch the isFinished() value instead.
//        wait();
//        Q_ASSERT(isFinished());
        while (!isFinished());
    }
}

template<class T, class... TArgs>
void
QThreadedUniquePtr<T, TArgs...>::Thread::run()
{
    // The subject is constructed here, inside the thread, to ensure that it lives
    // in the same thread as the objects it constructs.
    mSubjectUP = mAllocator();
    Q_ASSERT(mSubjectUP);
    // The subject is destructed here, inside the thread, to ensure that any
    // child-death events are sent from and to objects in the same thread.
//    connect(this, &QThread::finished, [this] { if (mTUP.mSubjectUP) mTUP.mSubjectUP.reset(); });
    QThread::run();
    if (mSubjectUP)
        mSubjectUP.reset();
}

#endif // QTHREADEDUNIQUEPTR_H

#ifndef NSFILEMANAGERSHIM_H
#define NSFILEMANAGERSHIM_H

#include <QtCore/QDateTime>
#include <QtCore/QFileDevice>

bool NSFileManagerShim_setFileTime(const QString& filePath, const QDateTime& newDate, QFileDevice::FileTime fileTime);

#endif // NSFILEMANAGERSHIM_H

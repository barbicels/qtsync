#ifndef NSBACKGROUNDTASKSHIM_H
#define NSBACKGROUNDTASKSHIM_H

#include <functional>

void NSBackgroundTaskShim_beginBackgroundTask(unsigned long& taskId, std::function<bool()> handler = nullptr);
void NSBackgroundTaskShim_endBackgroundTask(unsigned long taskId);
double NSBackgroundTaskShim_backgroundTimeRemaining();

#endif // NSBACKGROUNDTASKSHIM_H

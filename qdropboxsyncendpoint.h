#ifndef QDROPBOXSYNCENDPOINT_H
#define QDROPBOXSYNCENDPOINT_H

#include <dropbox/DropboxClient.h>
#include <dropbox/endpoint/DropboxAppInfo.h>
#include <dropbox/files/FilesDeletedMetadata.h>
#include <dropbox/files/FilesFileMetadata.h>
#include <dropbox/files/FilesFolderMetadata.h>
#include <memory>
#include "qfilesyncmanager.h"
#include "qabstractsyncendpoint.h"

using namespace dropboxQt;

class QTSYNC_EXPORT QDropboxSyncEndpoint : public QAbstractSyncEndpoint
{
    Q_OBJECT
public:
    explicit QDropboxSyncEndpoint(const QString& name, const QSyncEndpointData& data, QObject* parent = nullptr);
    virtual ~QDropboxSyncEndpoint() { }

    struct QTSYNC_EXPORT QDropboxSyncMetadata : public QAbstractSyncMetadata
    {
        friend class QDropboxSyncEndpoint;
        QString mFileRevOrFolderName;
    protected:
        virtual void serializeTo(QDataStream& stream) const override;
        virtual void serializeFrom(QDataStream& stream) override;
    };

    class QTSYNC_EXPORT QDropboxSyncEndpointState : public QAbstractSyncEndpointState
    {
        friend class QDropboxSyncEndpoint;
    public:
        explicit QDropboxSyncEndpointState();
        QDropboxSyncEndpointState(const QDropboxSyncEndpointState& other);
        virtual ~QDropboxSyncEndpointState() { }
    public:
        virtual std::unique_ptr<QAbstractSyncEndpointState> clone() const override;
    protected:
        enum QDropboxSyncEndpointStateVersion { Q_DROPBOX_SYNC_ENDPOINT_STATE_VERSION_1 = 1,
                                                Q_DROPBOX_SYNC_ENDPOINT_STATE_VERSION_2 = 2 };
        virtual void serializeTo(QDataStream& stream) const override;
        virtual void serializeFrom(QDataStream& stream) override;
        QString getDropboxAccountId() const;
        void setDropboxAccountId(const QString& dropboxAccountId = "");
        QString getDropboxAccountEmail() const;
        void setDropboxAccountEmail(const QString& dropboxAccountEmail = "");
        QString getDropboxRefreshToken() const;
        void setDropboxRefreshToken(const QString& dropboxRefreshToken = "");
        QString getDropboxAccessToken() const;
        void setDropboxAccessToken(const QString& dropboxAccessToken = "");
        QString getDropboxCursor() const;
        void setDropboxCursor(const QString& dropboxCursor = "");
        QMap<QString, QDropboxSyncMetadata>& getItemToItemMetadataMap();
    private:
        QString mDropboxAccountId;
        QString mDropboxAccountEmail;
        QString mDropboxRefreshToken;
        QString mDropboxAccessToken;
        QString mDropboxCursor;
        QMap<QString, QDropboxSyncMetadata> mItemToItemMetadataMap;
    };

    struct QTSYNC_EXPORT QDropboxSyncEndpointWorkItem : public QAbstractSyncEndpointWorkItem
    {
        std::unique_ptr<QByteArray> bufferArrayUP;
    };

public:
    virtual QString identity() const override;
    virtual bool dateLessThan(const QDateTime& date1, const QDateTime& date2) const override;
    virtual void getItemMetadata(const QString& item, QAbstractSyncMetadata& metadata) override;
    virtual std::shared_ptr<QAbstractSyncEndpointWorkItem> workItemFactory(const QString& item, const QAbstractSyncMetadata* metadataP) override;

protected:
    virtual bool engage(QSyncMode mode) override;
    virtual bool disengage() override;
    virtual bool activate() override { return true; }
    virtual bool deactivate() override { return true; }
    virtual bool report(const bool& abort) override;
    virtual void stopActivity() override;
    virtual void getFileMetadata(const QString& item, QFileSyncManager::QFileSyncMetadata& fileMetadata) const;
    virtual bool provideItem(QAbstractSyncEndpointWorkItem& workItem, const bool& abort) override;
    virtual bool consumeItem(QAbstractSyncEndpointWorkItem& workItem, const bool& abort) override;
    virtual QStringList getCanonicalPaths() override;
    virtual bool isReady() override;

private:
    bool isOffline();
    void getClient(const bool& abort);
    template<typename Routes, typename Result, typename... Args, typename... ArgsAsSupplied>
    Result guardedCall(const bool& abort, std::function<void()> setup, Routes* (DropboxClient::*gP)(), Result (Routes::*fP)(Args...), ArgsAsSupplied&&... a);

private:
//    constexpr static auto DROPBOX_ENDPOINT_NAME = "Dropbox";
    constexpr static auto DROPBOX_ENDPOINT_LOCALITY = 10;
    /* parameters */
    DropboxAppInfo mAppInfo;
    QString mAppKey;
    QString mAppSecret;
    QString mAppName;
    QString mRootPath;
    /* non-persistent state */
    std::unique_ptr<DropboxClient> mClientUP;
    std::map<QString, std::unique_ptr<const files::FileMetadata>> mItemToFileMetadataUPMap;
    std::map<QString, std::unique_ptr<const files::FolderMetadata>> mItemToFolderMetadataUPMap;
    std::map<QString, std::unique_ptr<const files::DeletedMetadata>> mItemToDeletedMetadataUPMap;
};

#endif // QDROPBOXSYNCENDPOINT_H

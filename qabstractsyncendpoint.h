#ifndef QABSTRACTSYNCENDPOINT_H
#define QABSTRACTSYNCENDPOINT_H

#include <deque>
#include <QtCore/QBuffer>
#include <QtCore/QDataStream>
#include <QtCore/QDebug>
#include <QtCore/QIODevice>
#include <QtCore/QMutex>
#include <QtCore/QObject>
#include "qabstractsyncmetadata.h"
#include "qsync.h"

class QTSYNC_EXPORT QAbstractSyncEndpoint : public QObject, public QMutex
{
    Q_OBJECT
public:
    class QAbstractSyncEndpointState;
    explicit QAbstractSyncEndpoint(const QString& name, unsigned int locality, QObject* parent = nullptr);
    virtual ~QAbstractSyncEndpoint() { }

    // QAbstractSyncEndpointState represents the endpoint's state at a point in time.
    // Depending on the type of endpoint, this can be a timestamp, a revision number,
    // an implementation-dependent cursor, a map of item states, or some combination.
    class QTSYNC_EXPORT QAbstractSyncEndpointState
    {
    public:
        virtual ~QAbstractSyncEndpointState() { }
    public:
        virtual std::unique_ptr<QAbstractSyncEndpointState> clone() const = 0;
        friend QDataStream& operator<<(QDataStream& stream, const QAbstractSyncEndpointState& state) { state.serializeTo(stream); return stream; }
        friend QDataStream& operator>>(QDataStream& stream, QAbstractSyncEndpointState& state) { state.serializeFrom(stream); return stream; }
    protected:
        enum QAbstractSyncEndpointStateVersion { Q_SYNC_ENDPOINT_STATE_VERSION_1 = 1 };
        virtual void serializeTo(QDataStream& stream) const;
        virtual void serializeFrom(QDataStream& stream);
    };

    struct QTSYNC_EXPORT QAbstractSyncEndpointWorkItem : public QMutex
    {
        QAbstractSyncEndpointWorkItem() : QMutex() { /*qDebug() << "[+]";*/ }
        virtual ~QAbstractSyncEndpointWorkItem() { /*qDebug() << "[-]";*/ }
        bool ready;
        bool canceled;  // signaled by provider endpoint
        const QAbstractSyncEndpoint* fromEndpointP;
        QString item;
        const QAbstractSyncMetadata* metadataP;
        std::unique_ptr<QIODevice> deviceUP;
    };

public slots:
    void engageEndpoint(QSyncMode mode);
    void disengageEndpoint();
    void activateEndpoint();
    void deactivateEndpoint();
    void reportEndpoint();
    void workEndpoint();
    void abortEndpoint();

signals:
    void endpointEngaged(bool success);
    void endpointDisengaged(bool success);
    void endpointReported(bool success);
    void endpointWorked(bool success);
    void endpointStateChanged();
    void endpointStateMismatched();
    void endpointError(const QString& endpoint, const QString& item, const QString& detail, bool* retry, bool* abort);
    void endpointQueueLengthChanged(const QString& endpoint, int queueLength);
    void openAuthorizeDialog(const QString& title, const QUrl& url);
    void closeAuthorizeDialog();
    void openProgressDialog(const QString& message, bool* success);
    void closeProgressDialog();

public:
    QString name() const { return mName; }
    unsigned int locality() const { return mLocality; }
    QMutex* accessMutex() const { return mAccessMutexP; }
    void setAccessMutex(QMutex* mutexP = nullptr) { mAccessMutexP = mutexP; }
    QSyncMode mode() const { return mMode; }
    QAbstractSyncEndpointState& state() { return *mStateUP; }
    const QAbstractSyncEndpointState& state() const { return *mStateUP; }
    void saveState() { mRollbackStateUP = mStateUP->clone(); Q_ASSERT(mRollbackStateUP); }
    void restoreState() { mStateUP = std::move(mRollbackStateUP); }
    const QStringList& canonicalPaths() const { return mCanonicalPaths; }
    const QStringList& reportedItems() const { return mReportedItems; }
    bool hasWorkItems();
    void enqueueWorkItem(const std::shared_ptr<QAbstractSyncEndpointWorkItem>& workItemSP);
    std::shared_ptr<QAbstractSyncEndpointWorkItem> dequeueWorkItem();
    void clearWorkItems();
    virtual QString identity() const = 0;
    virtual bool dateLessThan(const QDateTime& date1, const QDateTime& date2) const = 0;
    virtual void getItemMetadata(const QString& item, QAbstractSyncMetadata& metadata) = 0;
    virtual std::shared_ptr<QAbstractSyncEndpointWorkItem> workItemFactory(const QString& item, const QAbstractSyncMetadata* metadataP);

private:
    void reportQueueLength();

protected:
    virtual bool engage(QSyncMode mode) = 0;
    virtual bool disengage() = 0;
    virtual bool activate() = 0;
    virtual bool deactivate() = 0;
    virtual bool report(const bool& abort) = 0;
    virtual void stopActivity() = 0;
    virtual bool provideItem(QAbstractSyncEndpointWorkItem& workItem, const bool& abort) = 0;
    virtual bool consumeItem(QAbstractSyncEndpointWorkItem& workItem, const bool& abort) = 0;
    virtual QStringList getCanonicalPaths() = 0;
    virtual bool isReady() = 0;

protected:
    QString mName;
    unsigned int mLocality;
    QMutex* mAccessMutexP;
    QSyncMode mMode;
    bool mActivated;
    /* persistent state */
    std::unique_ptr<QAbstractSyncEndpointState> mStateUP;
    std::unique_ptr<QAbstractSyncEndpointState> mRollbackStateUP;
    /* non-persistent state */
    QStringList mCanonicalPaths;
    QStringList mReportedItems;
    bool mAbort;
    bool mWorkingItem;

private:
    std::deque<std::shared_ptr<QAbstractSyncEndpointWorkItem>> mWorkItemQueue;
};

namespace std
{
    template<>
    struct less<const QAbstractSyncEndpoint*>
    {
        bool operator()(const QAbstractSyncEndpoint* left, const QAbstractSyncEndpoint* right) const
        {
            return left->locality() < right->locality();
        }
    };
}

#endif // QABSTRACTSYNCENDPOINT_H

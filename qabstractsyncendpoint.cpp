#include "qabstractsyncendpoint.h"

QAbstractSyncEndpoint::QAbstractSyncEndpoint(const QString& name, unsigned int locality, QObject *parent)
    : QObject(parent), QMutex(),
      mName(name),
      mLocality(locality),
      mAccessMutexP(nullptr),
      mMode(SyncModeDisabled),
      mActivated(true),
      mAbort(false),
      mWorkingItem(false)
{
}

void
QAbstractSyncEndpoint::QAbstractSyncEndpointState::serializeTo(QDataStream& stream) const
{
    stream.setVersion(QDataStream::Qt_5_12);
    stream << (qint32) Q_SYNC_ENDPOINT_STATE_VERSION_1;
}

void
QAbstractSyncEndpoint::QAbstractSyncEndpointState::serializeFrom(QDataStream& stream)
{
    stream.setVersion(QDataStream::Qt_5_12);
    qint32 version;
    stream >> version;
    switch (version)
    {
    case 0: // null
        return;
    case Q_SYNC_ENDPOINT_STATE_VERSION_1:
        break;
    default:
        throw QSyncException(std::string("unknown sync endpoint state version: ") + std::to_string(version));
    }
}

void
QAbstractSyncEndpoint::engageEndpoint(QSyncMode mode)
{
//    if (mode == SyncModeDisabled)
//        return false;

    bool success = false;
    try
    {
        success = engage(mode);
    }
    catch (const std::exception &e)
    {
        bool retry = false;
        bool abort;
        emit endpointError(name(), QString(), e.what(), &retry, &abort);
    }
    if (!success)
    {
        emit endpointEngaged(false);
        return;
    }

    mMode = mode;
    mCanonicalPaths = getCanonicalPaths();

    // The following can cause a state-consistency problem if called during a sync
    // due to unexpected network issues or OAuth token expiry.  Best not to do this here.
    // It should be the client's responsibility NOT to persist state while a sync is running.
    emit endpointStateChanged();
    emit endpointEngaged(true);
}

void
QAbstractSyncEndpoint::disengageEndpoint()
{
    bool success = false;
    try
    {
        success = disengage();
    }
    catch (const std::exception &e)
    {
        bool retry = false;
        bool abort;
        emit endpointError(name(), QString(), e.what(), &retry, &abort);
    }
    if (!success)
    {
        emit endpointDisengaged(false);
        return;
    }

    mMode = SyncModeDisabled;

    // The following could cause a state-consistency problem if called during a sync
    // due to unexpected network issues or OAuth token expiry.  Best not to do this here.
    // It should be the client's responsibility NOT to persist state while a sync is running.
    emit endpointStateChanged();
    emit endpointDisengaged(true);
}

void
QAbstractSyncEndpoint::activateEndpoint()
{
    bool success = false;
    try
    {
        success = activate();
    }
    catch (const std::exception &e)
    {
        bool retry = false;
        bool abort;
        emit endpointError(name(), QString(), e.what(), &retry, &abort);
    }
    if (!success)
        return;

    mActivated = true;
}

void
QAbstractSyncEndpoint::deactivateEndpoint()
{
    bool success = false;
    try
    {
        success = deactivate();
    }
    catch (const std::exception &e)
    {
        bool retry = false;
        bool abort;
        emit endpointError(name(), QString(), e.what(), &retry, &abort);
    }
    if (!success)
        return;

    mActivated = false;
}

void
QAbstractSyncEndpoint::reportEndpoint()
{
    if (mMode == SyncModeDisabled)
    {
        emit endpointReported(false);
        mAbort = false;
        return;
    }
    mReportedItems = QStringList();
    mAbort = false;
    mWorkingItem = false;
    clearWorkItems();

    bool success = false;
    try
    {
        success = report(mAbort);
    }
    catch (const std::exception &e)
    {
        // Don't raise another error if the abort flag was set by abortEndpoint.
        if (!mAbort)
        {
            bool retry = false;
            bool abort;
            emit endpointError(name(), QString(), e.what(), &retry, &abort);
        }
    }
    if (mAbort)
        success = false;
    if (!success)
    {
        emit endpointReported(false);
        mAbort = false;
        return;
    }
    emit endpointReported(true);
}

std::shared_ptr<QAbstractSyncEndpoint::QAbstractSyncEndpointWorkItem>
QAbstractSyncEndpoint::workItemFactory(const QString& item, const QAbstractSyncMetadata* metadataP)
{
    auto const workItemSP = std::make_shared<QAbstractSyncEndpointWorkItem>();
    Q_ASSERT(workItemSP);
    auto& workItem = *workItemSP;
    workItem.ready = false;
    workItem.canceled = false;
    workItem.fromEndpointP = this;
    workItem.item = item;
    workItem.metadataP = metadataP;
    workItem.deviceUP = std::unique_ptr<QIODevice>();
    return workItemSP;
}

bool
QAbstractSyncEndpoint::hasWorkItems()
{
    QMutexLocker locker(this);
    return !mWorkItemQueue.empty();
}

void
QAbstractSyncEndpoint::enqueueWorkItem(const std::shared_ptr<QAbstractSyncEndpointWorkItem>& workItemSP)
{
    QMutexLocker locker(this);
    mWorkItemQueue.push_back(workItemSP);
    reportQueueLength();
}

std::shared_ptr<QAbstractSyncEndpoint::QAbstractSyncEndpointWorkItem>
QAbstractSyncEndpoint::dequeueWorkItem()
{
    QMutexLocker locker(this);
    // This must create another copy of the shared_ptr object, not just take a reference to it.
    auto const workItemSP = mWorkItemQueue.front();
    mWorkItemQueue.pop_front();
    reportQueueLength();
    return workItemSP;
}

void
QAbstractSyncEndpoint::clearWorkItems()
{
    QMutexLocker locker(this);
    mWorkItemQueue.clear();
    reportQueueLength();
}

void
QAbstractSyncEndpoint::reportQueueLength()
{
    emit endpointQueueLengthChanged(name(), mWorkItemQueue.size() + mWorkingItem);
}

void
QAbstractSyncEndpoint::workEndpoint()
{
    if (mMode == SyncModeDisabled)
    {
        emit endpointWorked(false);
        mAbort = false;
        return;
    }
    mAbort = false;
    while (hasWorkItems())
    {
        mWorkingItem = true;
        auto const workItemSP = dequeueWorkItem();
        auto& workItem = *workItemSP;
        QMutexLocker locker(&workItem);
        auto& deviceUP = workItem.deviceUP;
        if (workItem.fromEndpointP == this)
        {
            if (deviceUP)
                deviceUP->open(QIODevice::WriteOnly);
            forever
            {
                try
                {
                    // A false return means that the endpoint wants to cancel this work item.
                    if (!provideItem(workItem, mAbort))
                    {
                        qDebug() << "provider canceled item: " << workItem.item;
                        workItem.canceled = true;
                    }
                    break;
                }
                catch (const std::exception& e)
                {
                    // Don't raise another error if the abort flag was set by abortEndpoint.
                    if (mAbort)
                        break;
                    bool retry = false;
                    qDebug() << "failed to provide item: " << workItem.item;
                    emit endpointError(name(), workItem.item, e.what(), &retry, &mAbort);
                    if (!retry || mAbort)
                        break;
                    deviceUP->seek(0);
                }
            }
            if (deviceUP)
                deviceUP->close();
            // Do this last, to signal to consuming endpoints that this work item is ready.
            if (!mAbort)
                workItem.ready = true;
            mWorkingItem = false;
        }
        else if (workItem.ready)
        {
            if (!workItem.canceled)
            {
                if (deviceUP)
                    deviceUP->open(QIODevice::ReadOnly);
                forever
                {
                    try
                    {
                        // A false return means that the endpoint wants to ignore this work item.
                        if (!consumeItem(workItem, mAbort))
                            qDebug() << "consumer ignored item: " << workItem.item;
                        break;
                    }
                    catch (const std::exception& e)
                    {
                        // Don't raise another error if the abort flag was set by abortEndpoint.
                        if (mAbort)
                            break;
                        bool retry = false;
                        qDebug() << "failed to consume item: " << workItem.item;
                        emit endpointError(name(), workItem.item, e.what(), &retry, &mAbort);
                        if (!retry || mAbort)
                            break;
                        deviceUP->seek(0);
                    }
                }
                if (deviceUP)
                    deviceUP->close();
            }
            mWorkingItem = false;
        }
        else
        {
            mWorkingItem = false;
            enqueueWorkItem(workItemSP);
        }
        if (mAbort)
        {
            emit endpointWorked(false);
            mAbort = false;
            return;
        }
    }
    emit endpointStateChanged();
    emit endpointWorked(true);
}

void
QAbstractSyncEndpoint::abortEndpoint()
{
    // This slot is directly invoked from the calling thread.
    mAbort = true;
    stopActivity();
}

#include <QJniObject>
#include "AFileManagerShim.h"

bool
AFileManagerShim_setFileTime(const QString& filePath, const QDateTime& newDate, QFileDevice::FileTime fileTime)
{
    QJniObject convertedFilePath = QJniObject::fromString(filePath);
    const char* method =
            fileTime == QFileDevice::FileModificationTime ? "setLastModified" :
            nullptr;
    if (method == nullptr)
        return false;
//    QJniObject fileAttributeDate("java/util/Date", "(J)V", newDate.toMSecsSinceEpoch());
    QJniObject file("java/io/File", "(Ljava/lang/String;)V", convertedFilePath.object());
    bool success = (bool) file.callMethod<jboolean>(method, "(J)Z", newDate.toMSecsSinceEpoch());
    return success;
}
